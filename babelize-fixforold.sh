#!/bin/sh

# To run this script you have to have babel-cli and babel-preset-es2015 installed. In case the below paths do not match feel free to chnage them for your instance.

# babelize the source
rm babelized/*.js
~/node_modules/.bin/babel source -d babelized --presets es2015-ie
