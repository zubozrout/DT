#!/bin/sh

# To run this script you have to have JSDoc 3 installed. In case the below paths do not match feel free to chnage them for your instance.

rm -rf docs/
~/node_modules/.bin/jsdoc -c jsdoc.json --readme README.md --tutorials tutorials --destination "docs" source/*.js
