DT library (Atypic tests)
=========================

<p align="center">
  <img src="http://pedf.zubozrout.cz/draggable_grayscale.png" alt="Draggable queries" style="max-width: 100%"/>
</p>

## Synopsis

A DT library is a simple tool with the power to create various forms with (not only) draggable queries. It can be used for all sorts of things like tests for kids with a lot of images as well as more advanced tests for adults. That is where the powerful validation part of DT library comes into play with possible correct answer definitions based on regular strings, booleans, RegExps and a mixture of all these. However, this is only a client-side validation and therefore not adequate for computer experts who are able to get pass the security. However, although this part is not covered by this project, the whole validation process can be skipped and data parsed on a server.

The library is written in **JavaScript ES6** and on its own is not dependent on any additional technology except for some kind of a tool allowing draggable functionality. It has predefined functions to work with jQuery draggable and Interact.js (default), but you can easily extend it to support a different tool, even your own.

## Code Example

You can start by defining a new FormsHolder instance which allows you to have multiple forms on one page. It is also possible not to use this class and simply start with `new Form({data})`;

```
let formsholder = new FormsHolder();
				
let exampleform = formsholder.createForm({
	baseElement: document.getElementById("anElementWhereTheNewFormShouldBePut"),
	title: "Your Form Title",
	titleTagName: "h2",
	allowCorrections: true,
	customSubmit: false,
	scrollToError: true,
	type: "showSummary",
	submit: "Send",
	draggable: "interact"
});
```

Most attributes can be omitted. For more details on that see the project's documentation (see link below).

New query is then defined as follows (this is a simple text query example):

```
exampleform.newQueryItem({
	id: "name",
	type: "text",
	title: "What is your name?",
	subtitle: "Please fill in your name, like: \"John Mike\".",
	label: "Write your name here:",
	checkRules: "/^[a-zA-ZàáâäãåąčćęèéêëėįìíîïłńòóôöõøùúûüųūÿýżźñçčšžÀÁÂÄÃÅĄĆČĖĘÈÉÊËÌÍÎÏĮŁŃÒÓÔÖÕØÙÚÛÜŲŪŸÝŻŹÑßÇŒÆČŠŽ∂ð ,.'-]+$/u",
	required: true,
	error: "This is not a name, please try it again. The correct characters are UTF-8 only.",
	css: "background: #f44336; color: #fff;"
});
```

## Motivation

This project has been developed in order to make more advanced queries a simple thing to create. It should make it easier for teachers to create their own little tests for their kids. As it stands now the project is very well usable at kindergartens and elementary schools.

## Installation

Setting this tool up is pretty simple and straight forward. All you need is to include the minimized version of this library to a form page as well as the proffered draggable plugin.

It can look as simple as follows:
`<script src="interact.min.js"></script>`
`<script src="dt.min.js"></script>`

Also, this library doesn't enforce any custom CSS styles so it is completely up to you how will the elements look like. However, you should make the draggable options and input items properly sized so the options can be dragged into input items, potentially many option items next to each other. This all depends on what type of questions you are going to create.
If you want some inspiration or you don't care about he styles you can always use the ones from the library usage examples.

All you need to do after this is to create the form, as shown above or better in the example forms.

However, please note that the library stands on the JavaScript standard ECMAScript 6 and therefore doesn't work in older web browsers. Please make sure you use the latest versions of Firefox, Google Chrome/Chromium, Microsoft Edge and Safari. The library is also functional on mobile phones with some advanced web browsers, ie. Google Chrome.

## Support for pre-ES6 browsers

The library can be transformed to the obsolete JavaScript using Babel. To use it on the web, check out the dt-babelized.js that can be included on a page instead of the dt.min.js file. Check out the babelized example to see how it works.

Please note that albeit this works well eg. for native Android web browser it still won't work with IE. I don't have the resources nor the desire to introduce IE support.

## API Reference and documentation

You can find the project's documentation including tutorials online at this address: https://zubozrout.github.io/DT/index.html

## Tests

Some classes have automatic tests written. Those can be found in the /tests folder in the root of the project. Each html file in that folder is a written automatic test and you can run it simply by opening the file in your web browser.

## Contributors

The solely developer of this project is myself, but there are other people/organisations standing behind interact.js and Jasmine, whose files are both included in this project's repository.
* https://github.com/taye/interact.js
* https://github.com/jasmine/jasmine

## License

This project is available under GPLv3 license.
