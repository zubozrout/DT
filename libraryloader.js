"use strict";

class LibraryLoader {
	constructor(libraryStructure) {
		this.libraryStructure = libraryStructure || {
			directory: "source/",
			libraries: []
		};	
		return this;
	}
	
	load(callback) {
		if(this.libraryStructure.directory && this.libraryStructure.libraries) {
			let loadPromises = [];
			for(let i = 0; i < this.libraryStructure.libraries.length; i++) {
				loadPromises.push(this.loadSingle(this.libraryStructure.libraries[i]));
			}

			return Promise.all(loadPromises).then(() => {
				if(typeof callback !== typeof undefined && typeof callback === "function") {
					callback();
				}
				return "All scripts loaded";
			});
		}
		else {
			return Promise.reject(new Error("No scripts to load."));
		}
	}
	
	loadSingle(libraryName) {
		libraryName = libraryName || "";
		return new Promise((resolve, reject) => {
			let newScript = document.createElement("script");
			let scriptUrl = this.libraryStructure.directory + libraryName;
			newScript.setAttribute("src", scriptUrl);
			document.head.appendChild(newScript);
			
			newScript.addEventListener("load", () => {
				resolve(scriptUrl);
			});
			
			newScript.addEventListener("error", () => {
				reject(new Error("Loading " + scriptUrl + " failed"));
			});
		});
	}
}
