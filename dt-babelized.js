
"use strict";

/**
 * An abstract class containing some separate potentionally universal methods used in the DT library.
 * (It is not necessary for the class to be abstract but there is no point in instantiating it.)
 * @abstract
 */

var _typeof = typeof Symbol === "function" && typeof Symbol.iterator === "symbol" ? function (obj) { return typeof obj; } : function (obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; };

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var CommonFormFunctions = function () {
	/**
  * Create CommonFormFunctions.
  */
	function CommonFormFunctions() {
		_classCallCheck(this, CommonFormFunctions);

		/* http://stackoverflow.com/a/30560792 */
		if (false) {
			throw new TypeError("Cannot construct CommonFormFunctions instance, abstract class");
		}
		return this;
	}

	/**
  * Strip a DOM element of all its children.
  * @param {dom} element - A dom element to be emptied.
  * @return {number} - A number of child elements deleted.
  */


	CommonFormFunctions.empty = function empty(element) {
		var counter = 0;
		while (element.hasChildNodes()) {
			element.removeChild(element.lastChild);
			counter++;
		}
		return counter;
	};

	/**
  * Get DOM element's top, right, bottom and left borders, optionally counting in margins.
  * @param {dom} element - A dom element to have the borders measured.
  * @return {(Object|boolean)} - An object containing all DOM element positions, with keys: top, left, right, bottom - or false if no element was passed.
  */


	CommonFormFunctions.getElementPosition = function getElementPosition(element, countMargins) {
		if (element) {
			/* Get element's computed style property */
			var getStyle = function getStyle(property) {
				return window.getComputedStyle(element, null).getPropertyValue(property);
			};

			/* Get object with element positions {top, right, bottom, left} */
			var getPosition = function getPosition(element) {
				/* https://developer.mozilla.org/en-US/docs/Web/API/Element/getBoundingClientRect */
				return element.getBoundingClientRect();
			};

			var marginLeftRight = 0;
			var marginTopBottom = 0;
			if (countMargins === true) {
				/* Compute Element margins */
				marginLeftRight = parseFloat(getStyle("margin-left")) + parseFloat(getStyle("margin-right"));
				marginTopBottom = parseFloat(getStyle("margin-top")) + parseFloat(getStyle("margin-bottom"));
			}

			return {
				top: getPosition(element).top - marginLeftRight / 2,
				left: getPosition(element).left - marginTopBottom / 2,
				right: getPosition(element).right + marginLeftRight / 2,
				bottom: getPosition(element).bottom + marginTopBottom / 2
			};
		}
		return false;
	};

	/**
  * Get DOM element's top, right, bottom and left borders, counting it its margins, width and height.
  * Requires jQuery to run!
  * @param {dom} element - A dom element to have the borders measured.
  * @return {(Object|boolean)} - An object containing all DOM element positions, with keys: top, left, right, bottom - or false if no element was passed.
  */


	CommonFormFunctions.getElementPositionUsingjQuery = function getElementPositionUsingjQuery(element) {
		if (element) {
			element = $(element);
			var marginLeftRight = element.outerWidth(true) - element.width();
			var marginTopBottom = element.outerHeight(true) - element.height();
			return {
				top: element.position().top + marginLeftRight / 2,
				left: element.position().left + marginTopBottom / 2,
				right: element.position().left + element.innerWidth() + marginLeftRight / 2,
				bottom: element.position().top + element.innerHeight() + marginTopBottom / 2
			};
		}
		return false;
	};

	/**
  * Append inline style to a DOM object
  * @param {element} element - A dom element to be emptied.
  * @param {string} style - A style to be appended.
  * @return {boolean} - Return true if append was possible or false if element or syle is not defined.
  */


	CommonFormFunctions.appendStyle = function appendStyle(element, style) {
		if (element && style) {
			var originalStyle = element.getAttribute("style") ? element.getAttribute("style") : "";
			element.setAttribute("style", originalStyle + style);
			return true;
		}
		return false;
	};

	/**
  * Call to trigger an event on a DOM element with passed name and optionally data.
  * @param {element} eventElement - A dom element for the event to be called on.
  * @param {string} eventName - A name for the to-be-called event.
  * @param {(string|Object)} [eventData] - An optional attribute containing string or object with additional data to be passed to the event.
  * @return {boolean} - Return true if event creation was sccessful or false ig no eventElement was passed.
  */


	CommonFormFunctions.triggerEvent = function triggerEvent(eventElement, eventName, eventData) {
		eventName = eventName || "generalEvent";
		if (eventElement) {
			var event = null;
			if (eventData) {
				event = new CustomEvent(eventName, { detail: eventData });
			} else {
				event = new Event(eventName);
			}
			eventElement.dispatchEvent(event);
			return true;
		}
		return false;
	};

	/**
  * Confirm passed input type by returning it or default to the text.
  * Supports only the ones defined in the HTML standard.
  * {@link https://developer.mozilla.org/en-US/docs/Web/HTML/Element/input|More about input tag on Mozilla Developer Network}
  * @param {string} type - Input type to be checked.
  * @return {string} - Return the same input type passed or default to "text" if there was no match.
  */


	CommonFormFunctions.filterProperInputTypes = function filterProperInputTypes(type) {
		type = type || "";
		switch (type) {
			case "password":
				return "password";
			case "radio":
				return "radio";
			case "checkbox":
				return "checkbox";
			case "number":
				return "number";
			case "date":
				return "date";
			case "color":
				return "color";
			case "range":
				return "range";
			case "month":
				return "month";
			case "week":
				return "week";
			case "time":
				return "time";
			case "datetime-local":
				return "datetime-local";
			case "email":
				return "email";
			case "search":
				return "search";
			case "tel":
				return "tel";
			case "url":
				return "url";
			case "button":
				return "button";
			case "reset":
				return "reset";
			case "submit":
				return "submit";
			default:
				return "text";
		}
	};

	/**
  * Join two strings together.
  * @param {string} stringA - First string.
  * @param {string} stringB - Second string.
  * @param {string} [delimiter] - A delimiter to divide the two string parts. Default is comma.
  * @return {string} - Return the two strings joined, using the default or passed delimiter.
  */


	CommonFormFunctions.joinStrings = function joinStrings(stringA, stringB, delimiter) {
		if ((typeof delimiter === "undefined" ? "undefined" : _typeof(delimiter)) === undefined) {
			delimiter = ",";
		}

		var finalString = "";
		if (stringA) {
			finalString += stringA;
			if (stringB) {
				if (delimiter) {
					finalString += delimiter;
				}
				finalString += stringB;
			}
		}
		return finalString;
	};

	/**
  * Post passed data to a specified url
  * @param {string} url - Specify URL to submit the data to.
  * @param {string} data - Data to be submitted.
  * @return {Promise} - A promise for the XMLHttp submit request
  */


	CommonFormFunctions.postData = function postData(url, data) {
		return new Promise(function (resolve, reject) {
			var xhr = new XMLHttpRequest();
			xhr.onreadystatechange = function () {
				if (xhr.readyState == 4) {
					if (xhr.status == 200) {
						resolve(xhr.response);
					} else {
						reject(Error(xhr.statusText));
					}
				}
			};
			xhr.onerror = function () {
				reject(Error("Network Error"));
			};
			xhr.open("POST", url, true);
			xhr.send(data);
		});
	};

	/**
  * A generic function to make items draggable using any plugin defined.
  * @param {Object} data - Define draggable behaviour.
  * @param {dom} data.item - Defines the element which should be draggable.
  * @param {dom} [data.container] - Defines the outer element in which the dragging should be contained.
  * @param {function} data.plugin - Defines a function that should take care of draggable items.
  * @param {function} [data.onPosChange] - Defines a function that should be executed on draggable item position change.
  * @param {function} [data.onEnd] - Defines a function that should be executed once draggable event ends.
  * @return {mixed} - Returns plugin function result.
  */


	CommonFormFunctions.draggableSnippet = function draggableSnippet(data) {
		data = data || {};
		if (!data.item || !data.plugin) {
			throw "draggableSnippet must have draggable item and plugin defined!";
		}

		return data.plugin({
			item: data.item,
			container: data.container || null,
			plugin: data.plugin,
			onPosChange: data.onPosChange || null,
			onEnd: data.onEnd || null
		});
	};

	/**
  * jQuery draggable plugin, requires jQuery Draggable
  * @see {@link https://jqueryui.com/draggable/|jQuery Draggable}
  * @param {Object} data - Define draggable behaviour.
  * @param {dom} data.item - Defines the element which should be draggable.
  * @param {dom} [data.container] - Defines the outer element in which the dragging should be contained.
  * @param {function} [data.onEnd] - Defines a function that should be executed once draggable event ends.
  */


	CommonFormFunctions.draggablePlugin_jQuery = function draggablePlugin_jQuery(data) {
		data = data || {};
		if (!data.item) {
			throw "Plugin must have draggable item defined!";
		}

		if (typeof $ !== "function") {
			throw "jQuery must be loaded prior to using draggable in the DT library!";
		}

		if (typeof $().draggable !== "function") {
			throw "jQuery Draggable must be loaded prior to using draggable in the DT library!";
		}

		$(data.item).draggable({
			containment: data.container || null,
			stop: data.onEnd || null
		});
	};

	/**
  * jQuery draggable plugin, requires interact.js
  * @see {@link http://interactjs.io/|interact.js}
  * @param {Object} data - Define draggable behaviour.
  * @param {dom} data.item - Defines the element which should be draggable.
  * @param {dom} [data.container] - Defines the outer element in which the dragging should be contained.
  * @param {function} [data.onPosChange] - Defines a function that should be executed on draggable item position change.
  * @param {function} [data.onEnd] - Defines a function that should be executed once draggable event ends.
  */


	CommonFormFunctions.draggablePlugin_interact = function draggablePlugin_interact(data) {
		data = data || {};
		if (!data.item) {
			throw "Plugin must have draggable item defined!";
		}

		if (typeof interact !== "function") {
			throw "interact.js must be loaded prior to using draggable in the DT library!";
		}

		/* source: http://interactjs.io/ */
		interact(data.item).draggable({
			// keep the element within the area of it's parent
			restrict: {
				restriction: data.container || null,
				endOnly: false,
				elementRect: { top: 0, left: 0, bottom: 1, right: 1 }
			},

			// call this function on every dragmove event
			onmove: function onmove(event) {
				dragMoveListener(event);
				if (typeof data.onPosChange === "function") {
					data.onPosChange();
				}
			},
			// call this function on every dragend event
			onend: data.onEnd || null
		});

		function dragMoveListener(event) {
			var target = event.target,

			// keep the dragged position in the data-x/data-y attributes
			x = (parseFloat(target.getAttribute('data-x')) || 0) + event.dx,
			    y = (parseFloat(target.getAttribute('data-y')) || 0) + event.dy;

			// translate the element
			target.style.webkitTransform = target.style.transform = "translate(" + x + "px, " + y + "px)";

			// update the posiion attributes
			target.setAttribute("data-x", x);
			target.setAttribute("data-y", y);
		}

		// this is used later in the resizing and gesture demos
		window.dragMoveListener = dragMoveListener;

		return this;
	};

	return CommonFormFunctions;
}();"use strict";

/**
 * An abstract class containing some suited functionality widely used in other spedific DT classes.
 * @abstract
 */

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var CommonFormMethods = function () {
	/**
  * Create CommonFormMethods.
  */
	function CommonFormMethods() {
		_classCallCheck(this, CommonFormMethods);

		/* http://stackoverflow.com/a/30560792 */
		if (false) {
			throw new TypeError("Cannot construct CommonFormMethods instance directly, abstract class");
		}
		return this;
	}

	/**
  * Set the dom value.
  * @param {dom} object - Main DOM element assigned to this instance.
  */


	/**
  * Update position of this instance based on the dom set.
  * @return {boolean} - Return true if dom is set and obtaining position was possible or false if dom was not set.
  */
	CommonFormMethods.prototype.updatePosition = function updatePosition() {
		if (this.dom) {
			this.position = CommonFormFunctions.getElementPosition(this.dom, false);
			return true;
		}
		return false;
	};

	_createClass(CommonFormMethods, [{
		key: "dom",
		set: function set(object) {
			this.domModule = object;
		}

		/**
   * Set the position value.
   * @param {Object} position - Position Object with top, right, bottom and left keys to be assigned to this instance.
   */
		,


		/**
   * Get the dom value.
   * @return {dom} - The assigned dom value.
   */
		get: function get() {
			return this.domModule || null;
		}

		/**
   * Get the name of this instance based on id and index, if present.
   * @return {string} - The name.
   */

	}, {
		key: "position",
		set: function set(position) {
			this.optionPosition = position;
		}

		/**
   * Set the index value.
   * @param {number} index - Index value for this instance.
   */
		,


		/**
   * Get position.
   * @return {object} - Item's position, with keys top, right, bottom and left.
   */
		get: function get() {
			return this.optionPosition || null;
		}

		/**
   * Get index.
   * @return {(number|null)} - Item's index if set, or null.
   */

	}, {
		key: "index",
		set: function set(index) {
			this.indexVal = index;
		},
		get: function get() {
			return !isNaN(this.indexVal) ? this.indexVal : null;
		}

		/**
   * Find whether this items stands alone or is in the list with other items - as the assign type is.
   * @return {boolean} - Return true if this item is alone or false if there are others present as well.
   */

	}, {
		key: "name",
		get: function get() {
			if (this.queryData.type === "radio" || this.queryData.type === "checkbox") {
				return this.queryData.id || "id-undefined";
			} else if (!this.standsAlone) {
				return this.queryData.id + String(this.index) || "id-undefined";
			}
			return this.queryData.id || "id-undefined";
		}

		/**
   * Get the id name of this instance based on index, if present.
   * @return {string} - The unique id value.
   */

	}, {
		key: "htmlId",
		get: function get() {
			var formIndex = this.queryData.formIndex !== null ? this.queryData.formIndex + "-" : "";
			if (!this.standsAlone) {
				return formIndex + this.currentSubItem.htmlId || "id-undefined";
			}
			return formIndex + this.queryData.id || "id-undefined";
		}
	}, {
		key: "standsAlone",
		get: function get() {
			return this.queryData.items.length === 0 || this.index === null;
		}

		/**
   * Get QueryDataBlock data for current subitem - that is in case there are other items present, or get the whole QueryDataBlock.
   * @return {(Object|QueryDataBlock)} - Return object for a subitem or QueryDataBlock for a standalone item.
   */

	}, {
		key: "currentSubItem",
		get: function get() {
			if (!this.standsAlone && this.queryData.items.length > 0) {
				return this.queryData.items[this.index];
			}
			return this.queryData;
		}
	}]);

	return CommonFormMethods;
}();"use strict";

/**
 * Form holds all queries for a certain form, initiates valiadation, is able to make a valitation summary and takes care of things like title and form wrapping.
 */

var _typeof = typeof Symbol === "function" && typeof Symbol.iterator === "symbol" ? function (obj) { return typeof obj; } : function (obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; };

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var Form = function () {
	/**
  * Create Form. 
  * @param {Object} data - An object defining the properties of a Form.
  * @param {dom} data.baseElement - Tells the Form which DOM object it should use to insert itself.
  * @param {string} [data.title] - Defines a title content for the new Form.
  * @param {string} [data.titleTagName] - Defines a title tag name, like for example h1, h2, h3 or span, to be used in the new Form.
  * @param {boolean} [data.allowCorrections] - Tells whether corrections are enforced prior submitting. If set to true the form will only submit itself once all data is filled in correctly. If set to false the form will submit itself even with wrong answers set.
  * @param {boolean} [data.scrollToError] - If set to true the Form will scroll to the first wrong answer prior to submitting. This only works in combination with "data.allowCorrections = true".
  * @param {string} [data.type] - If set to "showSummary" it will display its own basic summary screen with the list of all input items and their answers after submit is complete.
  * @param {boolean} [data.customSubmit] - If set to true, a custom submit will be used instead of the default one.
  * @param {dom} [data.submitBtn] - If set and customSubmit set to true the passed one will be used instead.
  * @param {function} [data.submitEvent] - If defined the specified function will be called upon submiting the event with an event passed as an argument and this set to be an instance of this object.
  * @param {string} [data.submit] - Defines a value shown on a submit button.
  * @param {string} [data.submitUrl] - Defines a url on which a post XMLHttpRequest should be submited using POST method.
  * @param {(string|function)} [data.draggable] - Defines one of the default draggable plugins (jQuery|interact) or a custom one by passing a function. Default is interact.js.
  * @param {number} [index] - Defines index of the current form.
  */
	function Form(data, index) {
		var _this = this;

		_classCallCheck(this, Form);

		this.data = data || {};
		this.queries = [];
		this.sections = [];

		this.formIndex = index || null;

		this.scrollToError = this.data.scrollToError || false;

		/* Create container and embedd it into the baseElement or baseForm */
		this.container = document.createElement("div");
		this.container.classList.add("dt-form-container");
		if (this.data.baseElement) {
			this.data.baseElement.appendChild(this.container);
			this.baseForm = document.createElement("form");
			this.container.appendChild(this.baseForm);
			this.title = this.createTitle();
		} else if (this.data.baseForm) {
			this.baseForm = this.data.baseForm;
			this.baseForm.parentNode.insertBefore(this.container, this.baseForm.nextSibling);
			this.container.appendChild(this.baseForm);
		} else {
			throw "Neither data.baseElement nor data.baseForm has been defined. There is no element to place this Form to.";
		}

		/* Use custom submit button or create one */
		if (this.data.customSubmit !== true) {
			this.submit = this.createSubmit();
		} else if (this.data.submitBtn) {
			this.submit = this.data.submitBtn;
		}

		/* Set custom event if one is submitted */
		if (_typeof(this.data.submitEvent) !== undefined && typeof this.data.submitEvent === "function") {
			this.setOnSubmitEvent(this.data.submitEvent);
		}

		/* Default submit event */
		this.setOnSubmitEvent(function () {
			_this.submitProcess();
		});
	}

	/**
  * Get section container, that is this.baseForm which has been either passed upon initialization or created automatically.
  * @return {dom} - The baseForm DOM element.
  */


	/**
  * Add query to DOM on this form
  * @param {QueryItem} queryItem - A QueryItem to be logged in the internal structure and appended to the form element.
  * @return {(QueryItem|false)} - Returns the registerd QueryItem or false if no QueryItem was passed.
  */
	Form.prototype.addQuery = function addQuery(queryItem) {
		if (queryItem) {
			/* Create new section container for the query to sit in */
			var sectionContainer = document.createElement("div");
			sectionContainer.classList.add("dt-section-container", "dt-section-" + queryItem.queryData.index, queryItem.queryData.typeVal);

			/* If we created our own built in submit, then insert new query before it, otherwise don't care and just append to the form */
			if (this.submit) {
				this.submit.parentNode.insertBefore(sectionContainer, this.submit);
			} else {
				this.baseForm.appendChild(sectionContainer);
			}

			queryItem.queryData.sectionContainer = sectionContainer;
			this.queries[queryItem.queryData.index] = queryItem.write();

			this.sections.push(sectionContainer);
			return this.queries[queryItem.queryData.index];
		}
		return false;
	};

	/**
  * Create a new query (QueryItem + QueryDataBlock) on this form
  * @param {(QueryDataBlock|Object)} queryData - A QueryDataBlock to be used with the new QueryItem or an Object convertible to QueryDataBlock.
  * @see {@link QueryDataBlock} for more information on the required Object structure.
  * @return {(QueryItem|false)} - Returns the newly created QueryItem or false if no QueryData was passed.
  */


	Form.prototype.createQuery = function createQuery(queryData) {
		if (queryData) {
			/* Tell if passed data are already an instance of QueryDataBlock class, if not try to convert them */
			if (!(queryData instanceof QueryDataBlock)) {
				/* Converting query data to QueryDataBlock object */
				try {
					/* Create new QuertDataBlock and pass queryData to it. */
					queryData = new QueryDataBlock(queryData);
					/* Set new query index, based on the count of queries already present */
					queryData.index = this.queries.length;
					/* Pass Form index */
					queryData.formIndex = this.formIndex;
				} catch (error) {
					throw "Can't convert passed queryData Object inro QueryDataBlock.";
				}
			}

			queryData.baseForm = this.baseForm;
			queryData.draggable = this.data.draggable;

			return new QueryItem(queryData);
		}
		return false;
	};

	/**
  * Adds a new QueryItem to the Form
  * @param {(QueryDataBlock|Object)} queryData - A QueryDataBlock to be used with the new QueryItem or an Object convertible to QueryDataBlock, @see QueryDataBlock.
  * @return {(QueryItem|false)} - Returns the registerd QueryItem or false if no QueryItem was passed.
  */


	Form.prototype.newQueryItem = function newQueryItem(queryData) {
		var queryItem = this.createQuery(queryData);
		if (queryItem) {
			var newQuery = this.addQuery(queryItem);
			if (newQuery) {
				return newQuery;
			}
		}
		return false;
	};

	/* Returns an object with validation data summary containing an nentry for each QueryItem */
	/**
  * Gets validation summary object containing an array of relevant form submit data.
  * @return {object} - An object with the following structure: { items: [allValid, title, subtitle, type, queryValues], itemsLength }
  */


	Form.prototype.validationSummary = function validationSummary() {
		var summary = {
			itemsLengt: this.queries.length,
			items: []
		};
		for (var i = 0; i < this.queries.length; i++) {
			var validationData = this.queries[i].validate();
			summary.items[i] = {
				allValid: validationData.allValid,
				title: this.queries[i].queryData.title,
				subtitle: this.queries[i].queryData.subtitle,
				type: this.queries[i].queryData.type,
				queryValues: this.queries[i].value
			};
		}
		return summary;
	};

	/**
  * Checks whether the form is valid or not.
  * @return {boolean} - Return true if all input data is valid, false if not.
  */
	Form.prototype.isValid = function isValid() {
		for (var i = 0; i < this.queries.length; i++) {
			var validationData = this.queries[i].validate();
			if (!validationData.allValid) {
				return false;
			}
		}
		return true;
	};

	/**
  * Get first invalid input item.
  * @param {boolean} passAll - If set to false it won't go through each item and break on first error found.
  * @return {(Object|null)} - An object with the first invalid InputItem from the beginning and numeric queryIndex, or null if all items are valid.
  */


	Form.prototype.getFirstInvalidItem = function getFirstInvalidItem(passAll) {
		var firstInvalid = null;
		for (var i = 0; i < this.queries.length; i++) {
			var validationData = this.queries[i].validate();
			if (!validationData.allValid) {
				if (firstInvalid === null) {
					firstInvalid = {
						queryIndex: i,
						inputItem: validationData.inputItems[0]
					};
				}
				if (passAll === false) {
					break;
				}
			}
		}
		return firstInvalid;
	};

	/**
  * Validates all form fields by calling each QueryItem validation.
  * In addition scrolls to the first invalid element if there is such.
  * @return {boolean} - Returns true if all field are valid, false if not.
  */


	Form.prototype.validate = function validate() {
		var firstInvalidItem = this.getFirstInvalidItem(this.data.allowCorrections);

		if (this.scrollToError && firstInvalidItem !== null) {
			this.queries[firstInvalidItem.queryIndex].container.scrollIntoView(true);
			return false;
		}
		return firstInvalidItem === null;
	};

	/**
  * Returns an array of form input values.
  * @return {string[]} - Array of values for each registered each QueryItem.
  */
	Form.prototype.queryValues = function queryValues() {
		var queryValues = [];
		for (var i = 0; i < this.queries.length; i++) {
			queryValues.push(this.queries[i].value);
		}
		return queryValues;
	};

	/**
  * Serializes form data and returns them in a string.
  * @return {string} - Serialized Form data.
  */
	Form.prototype.serialize = function serialize() {
		var queryValues = this.queryValues();
		var string = "";
		for (var i = 0; i < queryValues.length; i++) {
			for (var j = 0; j < queryValues[i].length; j++) {
				if (queryValues[i][j].values != "") {
					if (j != 0) {
						string += ";";
					} else {
						if (i != 0 && string.length > 0) {
							string += "&";
						}
						string += queryValues[i][j].name + "=";
					}
					string += queryValues[i][j].values;
				}
			}
		}
		return string;
	};

	/**
  * Creates form title tag.
  * @return {dom} - The newly created form DOM title tag.
  */
	Form.prototype.createTitle = function createTitle() {
		var title = this.data.title ? this.data.title : "Form";
		var tagName = this.data.titleTagName ? this.data.titleTagName : "h2";

		var titleTag = document.createElement(tagName);
		titleTag.innerHTML = title;
		titleTag.classList.add("dt-main-title");
		this.baseForm.appendChild(titleTag);
		return titleTag;
	};

	/**
  * Creates form submit tag.
  * @return {dom} - The newly created form DOM submit button.
  */
	Form.prototype.createSubmit = function createSubmit() {
		var submitText = this.data.submit ? this.data.submit : "Submit";
		this.submitTag = document.createElement("input");
		this.submitTag.classList.add("dt-submit");
		this.submitTag.setAttribute("type", "submit");
		this.submitTag.value = submitText;
		this.baseForm.appendChild(this.submitTag);
		return this.submitTag;
	};

	/**
  * Define what should be called upon submitting the form using submit event.
  * Once the callback function is called it will receive the event via its parameter and this set to the instance of this Form class.
  * Always prevents default form submit event.
  * @param {function} call - A function that should be called upon submitting the form.
  * @return {boolean} - True if this.baseForm and call was defined and event set up, false if not.
  */


	Form.prototype.setOnSubmitEvent = function setOnSubmitEvent(callback) {
		var _this2 = this;

		if (this.baseForm && typeof callback === "function") {
			this.baseForm.addEventListener("submit", function (event) {
				event.preventDefault();
				callback.call(_this2, event);
			});
			return true;
		}
		return false;
	};

	/**
  * Walks trhough the full submit form process taking care of corrections if allowed and posting data using POST method to a specified url.
  * @return {boolean} - True if submit process completed or false if there were validation issues and corrections were allowed.
  */


	Form.prototype.submitProcess = function submitProcess() {
		var _this3 = this;

		var next = false;
		if (this.data.allowCorrections === false) {
			next = true;
		} else {
			if (this.validate()) {
				next = true;
			}
		}

		if (next) {
			if (this.data.submitUrl) {
				CommonFormFunctions.postData(this.data.submitUrl, this.serialize()).then(function (response) {
					_this3.container.removeChild(_this3.baseForm);
				}, function (error) {
					console.error("Error: " + error);
				});
			}

			if (this.data.type === "showSummary") {
				this.showSubmitSummary();
			}
		}

		return next;
	};

	/**
  * Show a simple built-in form submit summary in place of the original form.
  * @param {boolean} [keepForm] - If set to true the form will not be remove from the DOM and the submit summary will display below it.
  * @return {Form} - Instance of this Form.
  */


	Form.prototype.showSubmitSummary = function showSubmitSummary(keepForm) {
		if (keepForm !== true) {
			this.container.removeChild(this.baseForm);
		}

		var summary = this.validationSummary();
		var summaryItems = summary.items;
		var list = document.createElement("dl");
		list.classList.add("dt-summary-overview");
		for (var i = 0; i < summaryItems.length; i++) {
			var dt = document.createElement("dt");
			dt.classList.add("dt-query");
			list.appendChild(dt);
			var dtTitle = document.createElement("span");
			dtTitle.textContent = summaryItems[i].title;
			dtTitle.classList.add("dt-query-title");
			dt.appendChild(dtTitle);
			var ddsubtitle = document.createElement("dd");
			ddsubtitle.textContent = summaryItems[i].subtitle;
			ddsubtitle.classList.add("dt-query-subtitle");
			dt.appendChild(ddsubtitle);
			var ddvalid = document.createElement("dd");
			ddvalid.textContent = "Valid: " + (summaryItems[i].allValid === true ? "Yes" : "No");
			ddvalid.classList.add("dt-query-valid");
			ddvalid.classList.add(summaryItems[i].allValid === true ? "valid" : "invalid");
			dt.appendChild(ddvalid);
			var ddtype = document.createElement("dd");
			ddtype.textContent = "Question type: " + summaryItems[i].type;
			ddtype.classList.add("dt-query-type");
			dt.appendChild(ddtype);

			for (var j = 0; j < summaryItems[i].queryValues.length; j++) {
				var inputList = document.createElement("dl");
				inputList.classList.add("dt-input");
				var dtinputName = document.createElement("dt");
				dtinputName.textContent = "Input label or name: " + summaryItems[i].queryValues[j].label;
				dtinputName.classList.add("dt-input-name");
				inputList.appendChild(dtinputName);

				if (summaryItems[i].type === "checkbox" || summaryItems[i].type === "radio") {
					var ddchecked = document.createElement("dd");
					ddchecked.textContent = summaryItems[i].queryValues[j].values.join(", ") + " checked: " + (summaryItems[i].queryValues[j].checked === true ? "Yes" : "No");
					ddchecked.classList.add("dt-input-values");
					inputList.appendChild(ddchecked);
				} else if (summaryItems[i].queryValues[j].values.length > 0) {
					var ddinputValues = document.createElement("dd");
					ddinputValues.textContent = "Input values: " + summaryItems[i].queryValues[j].values.join(", ");
					ddinputValues.classList.add("dt-input-values");
					inputList.appendChild(ddinputValues);
				}
				var ddinputValid = document.createElement("dd");
				ddinputValid.textContent = "This input valid: " + (summaryItems[i].queryValues[j].valid === true ? "Yes" : "No");
				ddinputValid.classList.add("dt-input-valid");
				ddinputValid.classList.add(summaryItems[i].queryValues[j].valid === true ? "valid" : "invalid");
				inputList.appendChild(ddinputValid);
				dt.appendChild(inputList);
			}
		}

		/* Create retry a link button */
		var retry = document.createElement("a");
		retry.textContent = "Retry";
		retry.href = "#";
		retry.classList.add("dt-retry");
		retry.addEventListener("click", function (event) {
			event.preventDefault();
			location.reload();
		});
		list.appendChild(retry);

		/* Remove summary if already exists */
		if (this.summaryList) {
			this.summaryList.parentElement.removeChild(this.summaryList);
		}
		this.summaryList = list;

		/* Append new summary */
		this.container.appendChild(list);

		return this;
	};

	_createClass(Form, [{
		key: "sectionContainer",
		get: function get() {
			return this.baseForm;
		}
	}]);

	return Form;
}();"use strict";

/**
 * This class holds separate Form instances on one page.
 */

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var FormsHolder = function () {
	/**
  * Create FormsHolder.
  */
	function FormsHolder() {
		_classCallCheck(this, FormsHolder);

		this.forms = [];
	}

	/**
  * Creates a new Form object based on passed data.
  * @see {@link Form} for more information on the Object structure.
  * @param {Object} data - An object defining the properties of a Form.
  * @return {Form} - The new Form object created.
  */


	FormsHolder.prototype.createForm = function createForm(data) {
		data = data || {};
		var newForm = new Form(data, this.forms.length);
		this.addForm(newForm);
		return newForm;
	};

	/**
  * Loggs a form to the internal array.
  * @param {Form} form - A Form to be logged in an internal structure.
  * @return {boolean} - Return true if registering the Form was successful or false if it was already registered prior to this attempt.
  */


	FormsHolder.prototype.addForm = function addForm(form) {
		if (!this.isAlreadyLogged(form)) {
			this.forms.push(form);
			return true;
		}
		return false;
	};

	/**
  * Removes a form from the internal array.
  * @param {Form} form - A Form to be removed from the internal structure.
  * @return {boolean} - Return true if unregistering the Form was successful or false if it was already unregistered prior to this attempt.
  */


	FormsHolder.prototype.removeForm = function removeForm(form) {
		var index = this.formIndex(form);
		if (index >= 0) {
			this.forms.split(index, 1);
			return true;
		}
		return false;
	};

	/**
  * Determines whether the form is already logged or not
  * @param {Form} form - A Form to be found in the internal structure.
  * @return {bool} - If true the Form is registered in the structure. If false it is not.
  */


	FormsHolder.prototype.isAlreadyLogged = function isAlreadyLogged(form) {
		return this.formIndex(form) >= 0;
	};

	/**
  * Checks for the Form index in the internal structure.
  * @param {Form} form - A Form to be found in the internal structure.
  * @return {number} - Form index position, -1 means it is not present and numbers greater or equal 0 means the form is already present
  */


	FormsHolder.prototype.formIndex = function formIndex(form) {
		return this.forms.indexOf(form);
	};

	return FormsHolder;
}();"use strict";

/**
 * InputItem holds input DOM and takes care of its validation using QueryValidator.
 */

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _defaults(obj, defaults) { var keys = Object.getOwnPropertyNames(defaults); for (var i = 0; i < keys.length; i++) { var key = keys[i]; var value = Object.getOwnPropertyDescriptor(defaults, key); if (value && value.configurable && obj[key] === undefined) { Object.defineProperty(obj, key, value); } } return obj; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : _defaults(subClass, superClass); }

var InputItem = function (_CommonFormMethods) {
	_inherits(InputItem, _CommonFormMethods);

	/**
  * Create InputItemWrapper. 
  * @param {QueryDataBlock} queryData - An instance of QueryData.
  * @param {number} index - Order index of the input item in a query.
  */
	function InputItem(queryData, index) {
		var _ret;

		_classCallCheck(this, InputItem);

		if (!queryData) {
			throw new InputItemException("No data passed when creating a new instance of InputItemException");
		}
		if (!queryData instanceof QueryDataBlock) {
			throw new InputItemException("Data passed when creating new instance of InputItemException were not an instance of a QueryDataBlock");
		}

		var _this = _possibleConstructorReturn(this, _CommonFormMethods.call(this));

		_this.queryData = queryData;
		_this.index = index;

		_this.internalValue = {
			assignedOptions: [],
			text: "",
			checked: false
		};

		_this.valid = true;
		_this.associateCheckRule();

		return _ret = _this, _possibleConstructorReturn(_this, _ret);
	}

	/**
  * Creates InputItem DOM representation.
  * @return {InputItem} - This instance.
  */


	InputItem.prototype.createInput = function createInput() {
		this.dom = document.createElement("input");
		this.dom.classList.add("dt-section-container-input", "dt-section-container-input-type-" + this.queryData.type);
		if (this.queryData.direction) {
			this.dom.classList.add(this.queryData.direction);
		}
		this.dom.setAttribute("type", this.queryData.inputType);
		this.dom.setAttribute("id", this.htmlId);
		this.dom.setAttribute("name", this.name);

		if (this.queryData.type) {
			if (this.queryData.type === "order" || this.queryData.type === "assign") {
				this.restrictWriteAccess = true;
			}
		}

		if (!this.standsAlone) {
			if (this.queryData.properInputType) {
				if (this.queryData.properInputType === "radio" || this.queryData.properInputType === "checkbox") {
					this.dom.setAttribute("value", this.currentSubItem.id);
				}
			}
			CommonFormFunctions.appendStyle(this.dom, this.currentSubItem.css);
		}

		if (this.queryData.required) {
			this.dom.setAttribute("required", "");
		}

		return this;
	};

	/**
  * Associtate this InputItem with QueryValidator for its future validation.
  * Directly refferences to the QueryDataBlock's QueryValidator for this item.
  * @return {InputItem} - This instance.
  */


	InputItem.prototype.associateCheckRule = function associateCheckRule() {
		this.currentSubItem.queryValidator.itemToValidate = this;
		return this;
	};

	/**
  * Set if this InputItem's dom input is user editable.
  * @param {boolean} readOnly - Of true the input item won't be user editable - usable eg. on value assign queries.
  */


	/**
  * Validate data in this InputItem's DOM input = using adequate QueryValidator.
  * @return {InputItem} - This instance.
  */
	InputItem.prototype.validate = function validate() {
		if (this.dom) {
			this.valid = this.currentSubItem.queryValidator.validate();
			this.setValid();
		}
		return this.valid;
	};

	/**
  * Get all input item values, either the one typed in or all the assigned options.
  * @return {string[]} - An array containing all assigned values or a single inserted text value.
  */


	/**
  * Remove OptionItem from this instance of InputItem.
  * @return {InputItem} - This instance.
  */
	InputItem.prototype.deleteOption = function deleteOption(option) {
		var optionIndex = this.internalValue.assignedOptions.indexOf(option);
		if (optionIndex > -1) {
			this.internalValue.assignedOptions.splice(optionIndex, 1);
		}
		return this;
	};

	/**
  * Order OptionItems in InputItem by their position rather than the order they were dragged in (reorder).
  * Depends on Query direction value - "vertical" or "horizontal" - if no match found fallbacks to "horizontal".
  * @return {OptionItem[]} - Ordered Option items.
  */


	InputItem.prototype.orderAssignedOptions = function orderAssignedOptions() {
		for (var i = 0; i < this.internalValue.assignedOptions.length; i++) {
			this.internalValue.assignedOptions[i].updatePosition();
		}

		if (this.internalValue.assignedOptions && this.internalValue.assignedOptions.length > 1 && this.queryData.direction) {
			if (this.queryData.direction === "horizontal") {
				this.internalValue.assignedOptions.sort(function (a, b) {
					return a.position.left - b.position.left;
				});
			} else {
				this.internalValue.assignedOptions.sort(function (a, b) {
					return a.position.top - b.position.top;
				});
			}
		}

		return this.internalValue.assignedOptions;
	};

	/**
  * Modify InputItem DOM input value based on current this.internalValue.assignedOptions state.
  * @return {InputItem} - This instance.
  */


	InputItem.prototype.changeAssignedOptionsValue = function changeAssignedOptionsValue() {
		var finalValue = "";
		if (this.internalValue.assignedOptions && this.internalValue.assignedOptions.length > 0) {
			var ordered = this.orderAssignedOptions();
			for (var i = 0; i < ordered.length; i++) {
				finalValue += (i > 0 ? "&" : "") + ordered[i].scope.id;
			}
		}
		this.dom.value = finalValue;
		return this;
	};

	/**
  * Adds attribute with a value to InputItem's DOM input, like: <tagname attribute="value">.
  * @param {string} attribute - The name of the attribute.
  * @param {string} value - The value of the attribute.
  * @return {InputItem} - This instance.
  */


	InputItem.prototype.setAttribute = function setAttribute(attribute, value) {
		if (attribute != null && value != null) {
			this.dom.setAttribute(attribute, value);
		}
		return this;
	};

	/**
  * Changes this InputItem DOM input class based on the validity.
  * @param {bollean} [overwriteGraphicsTo] - If set to true, the InputItem will be force-set to valid.
  * @return {InputItem} - This instance.
  */
	InputItem.prototype.setValid = function setValid(overwriteGraphicsTo) {
		if (this.valid || overwriteGraphicsTo == true) {
			this.dom.classList.remove("invalid");
		} else {
			this.dom.classList.add("invalid");
		}
		return this;
	};

	/**
  * InputItem ValueChange event - call function on value change.
  * @param {function} callback - The function that should be executed on input's value change.
  * @return {InputItem} - This instance.
  */


	InputItem.prototype.onInputValueChange = function onInputValueChange(callback) {
		var _this2 = this;

		this.dom.addEventListener("change", function () {
			callback();
			_this2.setValid(true);
		});
		return this;
	};

	_createClass(InputItem, [{
		key: "restrictWriteAccess",
		set: function set(readOnly) {
			var readOnly = readOnly || false;
			this.dom.readOnly = readOnly;
		}
	}, {
		key: "value",
		get: function get() {
			if (this.internalValue.assignedOptions && this.internalValue.assignedOptions.length > 0) {
				this.orderAssignedOptions();

				var optionIds = [];
				for (var i = 0; i < this.internalValue.assignedOptions.length; i++) {
					optionIds.push(this.internalValue.assignedOptions[i].scope.validatorMatch);
				}
				return optionIds;
			}
			this.internalValue.text = this.dom.value;
			this.internalValue.checked = this.dom.checked;
			return this.internalValue.text ? [this.internalValue.text] : [];
		}

		/**
  * Returns obect with detailed information about this InputItem.
  * @return {Object} - {name:string - input item's name, label:string - input item's label or name if no label defined, values:string[], internalValue:string[] - assigned option's text, valid: boolean}
  */

	}, {
		key: "inputData",
		get: function get() {
			var options = [];
			for (var i = 0; i < this.internalValue.assignedOptions.length; i++) {
				options.push(this.internalValue.assignedOptions[i].scope.text);
			}
			return {
				name: this.name,
				label: this.currentSubItem.label || this.name,
				values: this.value,
				internalValue: options,
				valid: this.valid,
				checked: this.dom.checked
			};
		}

		/**
   * Assign OptionItem to this instance of InputItem.
   * @param {OptionItem} option - The OptionItem instance.
   */

	}, {
		key: "option",
		set: function set(option) {
			var optionIndex = this.internalValue.assignedOptions.indexOf(option);
			if (optionIndex === -1) {
				this.internalValue.assignedOptions.push(option);
			}
		}
	}]);

	return InputItem;
}(CommonFormMethods);

/**
 * Exception block for InputItem
 * @see {@link https://developer.mozilla.org/cs/docs/Web/JavaScript/Reference/Statements/throw|MDN Throw Statement}
 * @param {Object} message - Exception message Object.
 * @param {string} message.message - Message text.
 */


function InputItemException(message) {
	this.message = message;
	this.name = "InputItem";
}"use strict";

/**
 * InputItemWrapper wraps InputItem to take care of displaying input labels and possible errors.
 * It isn't responsible for the dragging functionality itself, but takses care of actions that are related to it.
 */

function _defaults(obj, defaults) { var keys = Object.getOwnPropertyNames(defaults); for (var i = 0; i < keys.length; i++) { var key = keys[i]; var value = Object.getOwnPropertyDescriptor(defaults, key); if (value && value.configurable && obj[key] === undefined) { Object.defineProperty(obj, key, value); } } return obj; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : _defaults(subClass, superClass); }

var InputItemWrapper = function (_CommonFormMethods) {
	_inherits(InputItemWrapper, _CommonFormMethods);

	/**
  * Create InputItemWrapper. 
  * @param {QueryDataBlock} queryData - An instance of QueryData.
  * @param {number} index - Order index of the input item in a query.
  */
	function InputItemWrapper(queryData, index) {
		_classCallCheck(this, InputItemWrapper);

		if (!queryData) {
			throw new InputItemWrapperException("No data passed when creating a new instance of InputItemWrapper");
		}
		if (!queryData instanceof QueryDataBlock) {
			throw new InputItemWrapperException("Data passed when creating new instance of InputItemWrapper were not an instance of a QueryDataBlock");
		}

		var _this = _possibleConstructorReturn(this, _CommonFormMethods.call(this));

		_this.queryData = queryData;
		_this.index = !isNaN(index) ? index : null;
		_this.input = null;
		_this.errorTag = null;

		_this.valid = true;
		return _this;
	}

	/**
  * Creates InputItem and assigns it into this InputItemWrapper.
  * @return {InputItemWrapper} - This instance.
  */


	InputItemWrapper.prototype.createInput = function createInput() {
		this.dom = document.createElement("div");
		this.dom.classList.add("dt-section-container-wrapper");
		this.createLabel();

		this.input = new InputItem(this.queryData, this.index).createInput();
		this.input.validation = this.checkRule;
		this.dom.appendChild(this.input.dom);
		this.createErrorTag();

		return this;
	};

	/**
  * Creates DOM label for this InputItemWrapper.
  * @return {InputItemWrapper} - This instance.
  */


	InputItemWrapper.prototype.createLabel = function createLabel() {
		var containerLabel = this.queryData.label;
		var itemLabel = this.queryData.items.length > 0 && this.index != null;

		if (containerLabel || itemLabel) {
			var labelTag = document.createElement("label");

			if (containerLabel) {
				labelTag.classList.add("dt-section-container-label", this.queryData.type);
				labelTag.textContent = this.queryData.label;
			}
			if (itemLabel) {
				labelTag.textContent = this.queryData.items[this.index].label;
			}

			labelTag.setAttribute("for", this.htmlId);
			this.dom.appendChild(labelTag);
		}

		return this;
	};

	/*  */
	/**
  * Creates Input Error Message DOM.
  * @return {InputItemWrapper} - This instance.
  */


	InputItemWrapper.prototype.createErrorTag = function createErrorTag() {
		var _this2 = this;

		if (this.queryData.error) {
			this.errorTag = document.createElement("span");
			this.errorTag.classList.add("dt-section-container-error");
			this.dom.appendChild(this.errorTag);

			this.input.onInputValueChange(function () {
				_this2.setValid(true);
			});
		}

		return this;
	};

	/**
  * Validates this InputItemWrapper, requests InputItem validation.
  * @return {(Object|false)} - Data validation summary from the assigned InputItem {valid:boolean, data:inputData, inputWrapper:this, inputItem:InputItem} or false if none assigned.
  */


	InputItemWrapper.prototype.validate = function validate() {
		if (this.input) {
			var valid = this.input.validate();
			this.setValid();

			return {
				valid: valid,
				data: this.input.inputData,
				inputWrapper: this,
				inputItem: this.input
			};
		}
		return false;
	};

	/**
  * Displays validation state using the errorTag, setting or unsetting classnames for them to by styllable.
  * @param {boolean} [overwriteGraphicsTo] - If set to true, the InputItemWrapper will be force-set to valid.
  * @return {OptionItem} - This instance.
  */


	InputItemWrapper.prototype.setValid = function setValid(overwriteGraphicsTo) {
		if (this.input.valid || overwriteGraphicsTo == true) {
			if (this.queryData.error && this.errorTag) {
				this.errorTag.innerText = "";
			}
			this.dom.classList.remove("invalid");
		} else {
			if (this.errorTag) {
				this.errorTag.innerText = this.currentSubItem.error || this.queryData.error || "Bad content";
			}
			this.dom.classList.add("invalid");
		}

		return this;
	};

	return InputItemWrapper;
}(CommonFormMethods);

/**
 * Exception block for InputItemWrapper
 * @see {@link https://developer.mozilla.org/cs/docs/Web/JavaScript/Reference/Statements/throw|MDN Throw Statement}
 * @param {Object} message - Exception message Object.
 * @param {string} message.message - Message text.
 */


function InputItemWrapperException(message) {
	this.message = message;
	this.name = "InputItemWrapper";
}"use strict";

/**
 * OptionItem takes care of Draggable option elements on page.
 * It isn't responsible for the dragging functionality itself, but takses care of actions that are related to it.
 */

var _typeof = typeof Symbol === "function" && typeof Symbol.iterator === "symbol" ? function (obj) { return typeof obj; } : function (obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; };

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _defaults(obj, defaults) { var keys = Object.getOwnPropertyNames(defaults); for (var i = 0; i < keys.length; i++) { var key = keys[i]; var value = Object.getOwnPropertyDescriptor(defaults, key); if (value && value.configurable && obj[key] === undefined) { Object.defineProperty(obj, key, value); } } return obj; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : _defaults(subClass, superClass); }

var OptionItem = function (_CommonFormMethods) {
	_inherits(OptionItem, _CommonFormMethods);

	/**
  * Create OptionItem. 
  * @param {QueryDataBlock} queryData - An instance of QueryData.
  * @param {number} index - Order index of the option item in a query.
  */
	function OptionItem(queryData, index) {
		var _ret;

		_classCallCheck(this, OptionItem);

		if (!queryData) {
			throw new OptionItemException("No data passed when creating a new instance of OptionItem");
		}
		if (!queryData instanceof QueryDataBlock) {
			throw new OptionItemException("Data passed when creating new instance of OptionItem were not an instance of a QueryDataBlock");
		}

		var _this = _possibleConstructorReturn(this, _CommonFormMethods.call(this));

		_this.queryData = queryData;
		_this.inputItem = null;
		_this.index = index;

		return _ret = _this, _possibleConstructorReturn(_this, _ret);
	}

	/**
  * Gets inputItem.
  * @return {InputItem} - An InputItem object instance related to this OptionItem.
  */


	/**
  * Creates a dom representation of this OptionItem.
  * @return {OptionItem} - This instance.
  */
	OptionItem.prototype.createOption = function createOption() {
		this.dom = document.createElement("div");
		this.dom.classList.add("dt-section-container-option-" + String(this.index));
		this.dom.setAttribute("id", this.scope.htmlId);
		this.dom.textContent = this.scope.text;
		CommonFormFunctions.appendStyle(this.dom, this.scope.css);

		return this;
	};

	/**
  * Removes assigned InputItem from an OptionItem instance.
  * Eg. if this option item has been moved outside of the dom input element.
  * @return {OptionItem} - This instance.
  */


	OptionItem.prototype.deleteInput = function deleteInput() {
		if (this.input) {
			var optionIndex = this.inputItem.internalValue.assignedOptions.indexOf(this);
			this.inputItem.deleteOption(this);
			this.inputItem = null;

			this.dom.classList.add("is-out-of-container");
			this.dom.classList.remove("is-inside-container");
			return true;
		}
		return false;
	};

	/**
  * Supposed to be called on OptionItem position change (user drag) and calls the other necessary methods.
  * @param {InputItem} [inputItem] - If set to the instance of InputItem it will be assigned to this QueryItem an registered as "beign in its scope".
  * @return {OptionItem} - This instance.
  */


	OptionItem.prototype.positionChanged = function positionChanged(inputItem) {
		this.dom.classList.remove("dt-option-reset");

		// Backup revious input before modifying it.
		var originalInput = this.input || null;

		if ((typeof inputItem === "undefined" ? "undefined" : _typeof(inputItem)) !== (typeof undefined === "undefined" ? "undefined" : _typeof(undefined)) && inputItem instanceof InputItem) {
			this.input = inputItem;
			inputItem.changeAssignedOptionsValue();
		}

		if (originalInput !== inputItem && originalInput instanceof InputItem) {
			originalInput.deleteOption(this);
			originalInput.changeAssignedOptionsValue();
		}

		if (!inputItem) {
			this.deleteInput();
		}

		return this;
	};

	_createClass(OptionItem, [{
		key: "input",
		get: function get() {
			return this.inputItem;
		}

		/**
   * Gets data Object for this OptionItem.
   * @return {Object} - This option data from QueryDataBlock instance.
   */
		,


		/**
   * Sets InputItem to be related with this instance of OptionItem.
   * Eg. if this option item has been moved over the dom input element.
   * @param {InputItem} inputItem - The InputItem instance.
   */
		set: function set(inputItem) {
			this.inputItem = inputItem;
			this.inputItem.option = this;

			this.dom.classList.add("is-inside-container");
			this.dom.classList.remove("is-out-of-container");
		}
	}, {
		key: "scope",
		get: function get() {
			return this.queryData.options[this.index];
		}
	}]);

	return OptionItem;
}(CommonFormMethods);

/**
 * Exception block for OptionItem
 * @see {@link https://developer.mozilla.org/cs/docs/Web/JavaScript/Reference/Statements/throw|MDN Throw Statement}
 * @param {Object} message - Exception message Object.
 * @param {string} message.message - Message text.
 */


function OptionItemException(message) {
	this.message = message;
	this.name = "OptionItem";
}"use strict";

/**
 * QueryDataBlock class holds all data relevant for a single section/query (QueryItem), includin InputItems and OptionItems.
 */

var _typeof = typeof Symbol === "function" && typeof Symbol.iterator === "symbol" ? function (obj) { return typeof obj; } : function (obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; };

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var QueryDataBlock = function () {
	/**
  * Create QueryDataBlock. 
  * @param {Object} data - Defines QueryItem's data (data for a specific form section).
  * @param {string} [data.formIndex] - Current form index used for unique ids.
  * @param {string} [data.id] - Section id.
  * @param {string} [data.type] - Query type, default is "text".
  * @param {string} [data.title] - Query title text.
  * @param {string} [data.subtitle] - Query subtitle text.
  * @param {string} [data.label] - Query label text.
  * @param {string} [data.error] - Query error text.
  * @param {string} [data.direction] - Query direction, either "vertical" or "horizontal".
  * @param {string} [data.optionsOrder] - Query options order, either "before" or "after" the input items.
  * @param {boolean} [data.required] - Sets query filled-in value to be required. Default is false.
  * @param {string} [data.css] - Query specific css string.
  * @param {(dom|string)} [data.before] - Defines DOM object or a string as text or html to be placed at the beginning of the query.
  * @param {(dom|string)} [data.after] - Defines DOM object or a string as text or html to be placed at the end of the query.
  * @param {(boolean|string|RegExp|boolean[]|string[]|RegExp[])} [data.checkRules] - Defines possible validation queries. If array passed value must match them all.
  * @param {string} [data.arrayMatch] - If set to "and" it will require input value to have a match, if "or" (default) at least one input value will have to have a match.
  * @param {Object[]} [data.items] - Defines input items for this Query.
  * @param {string} [data.items.id] - Defines input items' ids for this Query.
  * @param {string} [data.items.label] - Defines input items' labels for this Query.
  * @param {string} [data.items.css] - Defines input items' css strings for this Query.
  * @param {string} [data.items.error] - Defines input items' error values - if none set defaults to the general QueryDataBlock error value.
  * @param {(boolean|string|RegExp|boolean[]|string[]|RegExp[])} [data.items.checkRules] - Defines input items for this Query.
  * @param {string} [data.items.arrayMatch] - If set to "and" it will require input value to have a match, if "or" (default) at least one input value will have to have a match.
  * @param {Object[]} [data.options] - Defines option items for this Query.
  * @param {string} [data.options.id] - Defines option items' ids for this Query - will be matched with data.items.checkRules.
  * @param {string} [data.options.text] - Defines option items' text strings for this Query.
  * @param {string} [data.options.css] - Defines option items' css strings for this Query.
  */
	function QueryDataBlock(data) {
		_classCallCheck(this, QueryDataBlock);

		this.data = data || {};

		/* Count of existing QueryDataBlocks */
		QueryDataBlock.count = (QueryDataBlock.count || 0) + 1;
		this.uniqueInstanceId = QueryDataBlock.count;

		this.formIndex = data.formIndex || null;

		this.idVal = this.createUniqueId(this.data.id) || "";
		this.typeVal = this.data.type || "text";
		this.titleVal = this.data.title || null;
		this.subtitleVal = this.data.subtitle || null;
		this.labelVal = this.data.label || null;
		this.errorVal = this.data.error || "This field has not been correctly filled in.";
		this.directionVal = this.data.direction || null;
		this.optionsOrderVal = this.data.optionsOrder || null;
		this.requiredVal = this.data.required || false;
		this.cssVal = this.data.css || null;

		this.before = this.data.before || null;
		this.after = this.data.after || null;

		this.items = this.data.items || [];
		this.options = this.data.options || [];

		this.queryValidator = new QueryValidator(this.data.checkRules);
		this.queryValidator.setMatch(this.data.arrayMatch || "or");
		this.setProperInputType();

		return this;
	}

	/**
  * Get this query item's type.
  * @return {string} - query type.
  */


	/**
  * Creates a unique id for this QuerydataBlock, based on an uniqu InstanceID.
  * @param {string} [id] - A string to be connected with the unique instance number to create a unique id.
  */
	QueryDataBlock.prototype.createUniqueId = function createUniqueId(id) {
		id = id || "";
		return CommonFormFunctions.joinStrings(this.uniqueInstanceId, id, "_");
	};

	/**
  * Filters all unsupported input types, picks a default if a wrong input type has been passed.
  * @return {QueryDataBlock} - Returns this QueryDataBlock instance.
  */


	QueryDataBlock.prototype.setProperInputType = function setProperInputType() {
		this.inputType = CommonFormFunctions.filterProperInputTypes(this.type);
		return this;
	};

	/**
  * Returns a DOM with "dt-custom-form-content" class name, accepts HTML/text strings as well as DOM structures.
  * @param {(dom|string)} [element] - A dom object or a string - as text or HTML.
  * @param {string} [customClass] - Custom class name to be added to the final element.
  * @return {dom} - An updated or newly created DOM object.
  */


	QueryDataBlock.prototype.createCustomDomContent = function createCustomDomContent(element, customClass) {
		if (element) {
			var finalDom = null;
			if (element.nodeType && element.nodeType === 1) {
				;
				element.classList = [];
				finalDom = element;
			} else {
				finalDom = document.createElement("div");
				finalDom.innerHTML = element;
			}

			finalDom.classList.add("dt-custom-form-content");
			if (customClass) {
				finalDom.classList.add(customClass);
			}
			return finalDom;
		}
		return null;
	};

	_createClass(QueryDataBlock, [{
		key: "type",
		get: function get() {
			return this.typeVal;
		}

		/**
   * Get this query item's id.
   * @return {string} - query id.
   */

	}, {
		key: "id",
		get: function get() {
			return this.idVal;
		}

		/**
   * Get this query item's title.
   * @return {string} - query title.
   */

	}, {
		key: "title",
		get: function get() {
			return this.titleVal;
		}

		/**
   * Get this query item's subtitle.
   * @return {string} - query subtitle.
   */

	}, {
		key: "subtitle",
		get: function get() {
			return this.subtitleVal;
		}

		/**
   * Get this query item's label.
   * @return {string} - query label.
   */

	}, {
		key: "label",
		get: function get() {
			return this.labelVal;
		}

		/**
   * Get this query item's error.
   * @return {string} - query error text.
   */

	}, {
		key: "error",
		get: function get() {
			return this.errorVal;
		}

		/**
   * Get this query item's direction. Applicable only for draggable queries.
   * @return {string} - query direction - as "horizontal" or "vertical".
   */

	}, {
		key: "direction",
		get: function get() {
			return this.directionVal;
		}

		/**
   * Find out whether this query fille-in valueis required or if it is optional.
   * @return {boolen} - Returns true if this query is required, false if not.
   */

	}, {
		key: "required",
		get: function get() {
			return this.requiredVal;
		}

		/**
   * Get this query item's css.
   * @return {string} - query css string.
   */

	}, {
		key: "css",
		get: function get() {
			return this.cssVal;
		}

		/**
   * Get this query item's index.
   * @return {(string|null)} - query index (order number) in form or null if no index found.
   */

	}, {
		key: "index",
		get: function get() {
			if (!isNaN(this.indexVal)) {
				return this.indexVal;
			}
			return null;
		}

		/**
   * Get this query item's items.
   * @return {Object[]} - Array containing object data of each input item.
   */
		,


		/**
   * Set index.
   * @param {number} value - Set this item's index value.
   */
		set: function set(value) {
			this.indexVal = value;
			if (!this.id) {
				this.idVal = this.indexVal;
			}
		}

		/**
   * Set baseForm DOM element.
   * @param {dom} baseForm - DOM element holding all form fields.
   */

	}, {
		key: "items",
		get: function get() {
			if (_typeof(this.itemsVal) !== (typeof undefined === "undefined" ? "undefined" : _typeof(undefined))) {
				return this.itemsVal;
			}
			return [];
		}

		/**
   * Get this query item's options.
   * @return {Object[]} - Array containing object data of each option item.
   */
		,


		/**
   * Takes an array of items (optionally) and generates an Array of Objects, each Object for one item (InputItemWrapper).
   * @param {Object[]} [items] - Array with input items data.
   * @see {@link constructor} for more information.
   */
		set: function set(items) {
			items = items || [];
			this.itemsVal = [];
			for (var i = 0; i < items.length; i++) {
				this.itemsVal[i] = {};
				this.itemsVal[i].id = items[i].id;
				this.itemsVal[i].htmlId = "input-" + this.id + "-" + items[i].id;
				this.itemsVal[i].label = items[i].label || "";
				this.itemsVal[i].text = items[i].text || "";
				this.itemsVal[i].css = items[i].css || "";
				this.itemsVal[i].error = items[i].error || null;
				this.itemsVal[i].queryValidator = new QueryValidator(items[i].checkRules);
				this.itemsVal[i].queryValidator.setMatch(items[i].arrayMatch || "or");
			}
		}

		/**
   * Takes an array of options (optionally) and generates an Array of Objects, each Object for one option (OptionItem).
   * @param {Object[]} [options] - Array with option items data.
   * @see {@link constructor} for more information.
   */

	}, {
		key: "options",
		get: function get() {
			return this.optionsVal;
		}

		/**
   * Get this query item's options order.
   * @return {string} - Either "before" if option items are rendered above input items or "after" is otherwise.
   */
		,
		set: function set(options) {
			options = options || [];
			this.optionsVal = [];
			for (var i = 0; i < options.length; i++) {
				this.optionsVal[i] = {};
				this.optionsVal[i].id = options[i].id;
				this.optionsVal[i].htmlId = "option-" + this.id + "-" + options[i].id;
				this.optionsVal[i].validatorMatch = options[i].id;
				this.optionsVal[i].text = options[i].text || "";
				this.optionsVal[i].css = options[i].css || "";
			}
		}

		/**
   * Sets dom which preceeds the query once rendered.
   * @param {dom} element - An element to be placed at the beggining of this query.
   */

	}, {
		key: "optionsOrder",
		get: function get() {
			return this.optionsOrderVal;
		}

		/**
   * Get this query item's base form DOM object.
   * @return {dom} - An object in which all form elements including section queries are placed.
   */

	}, {
		key: "baseForm",
		get: function get() {
			if (this.baseFormVal) {
				return this.baseFormVal;
			}
			return null;
		}

		/**
   * Get this query item's section DOM container.
   * @return {dom} - Query container.
   */
		,
		set: function set(baseForm) {
			this.baseFormVal = baseForm;
		}

		/**
   * Set sectionContainer DOM element.
   * @param {dom} sectionContainer - DOM element contaning this query.
   */

	}, {
		key: "sectionContainer",
		get: function get() {
			if (this.sectionContainerVal) {
				return this.sectionContainerVal;
			}
			return null;
		}

		/**
   * Get proper input type, filtered to a know types only in case unsupported type has been passed.
   * @return {string} - A proper input type.
   */
		,
		set: function set(sectionContainer) {
			this.sectionContainerVal = sectionContainer;
		}
	}, {
		key: "properInputType",
		get: function get() {
			return this.inputType;
		}

		/**
   * Get before section dom.
   * @return {dom} - A custom element being inserted at the beginning of this query.
   */

	}, {
		key: "before",
		get: function get() {
			return this.beforeDOM;
		}

		/**
   * Get after section dom.
   * @return {dom} - A custom element being inserted at the end of this query.
   */
		,
		set: function set(element) {
			this.beforeDOM = this.createCustomDomContent(element, "before");
		}

		/**
   * Sets dom which follows the query once rendered.
   * @param {dom} element - An element to be placed at the end of this query.
   */

	}, {
		key: "after",
		get: function get() {
			return this.afterDOM;
		},
		set: function set(element) {
			this.afterDOM = this.createCustomDomContent(element, "after");
		}
	}]);

	return QueryDataBlock;
}();"use strict";
/* QueryItem Class */

/**
 * This class holds query and contains InputItemWrappers and their InputItems and OptionItems for draggable queries.
 */

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var QueryItem = function () {
	/**
  * Create QueryItem.
  * @param {QueryDataBlock} queryData - A QueryDataBlock instance holding all relevant query data.
  */
	function QueryItem(queryData) {
		_classCallCheck(this, QueryItem);

		if (!queryData) {
			throw new QueryItemException("No data passed when creating a new instance of QueryItem");
		}
		if (!queryData instanceof QueryDataBlock) {
			throw new QueryItemException("Data passed when creating new instance of QueryItem were not an instance of a QueryDataBlock");
		}

		this.queryData = queryData;

		this.inputWrappers = [];
		this.inputItems = [];
		this.optionItems = [];

		this.data = {};
		this.errorTag = null;

		this.optionsData = {};
		this.optionsContainer = this.data.optionsContainer || null;

		/* Define draggable plugin */
		this.draggable = this.queryData.draggable || null;
	}

	/**
  * Check if OptionItem it is in any of the InputItems of this QueryItem.
  * @param {OptionItem} optionItem - An OptionItem which position should be found.
  * @param {number} [optionalMargin] - Margin to be taken taken in account while matching OptionItem's position on InputItem positions.
  * @return {(InputItems|false)} - Return matched InputItem instance or false if none found.
  */


	QueryItem.prototype.checkIfOptionItemIsInsideInput = function checkIfOptionItemIsInsideInput(optionItem, optionalMargin) {
		optionItem.updatePosition();
		for (var i = 0; i < this.inputItems.length; i++) {
			/* Set default margin to 0 */
			optionalMargin = optionalMargin || 0;

			if (this.inputItems[i] && optionItem) {
				this.inputItems[i].updatePosition();

				/* Element inside vertically */
				if (optionItem.position.top > this.inputItems[i].position.top && optionItem.position.bottom < this.inputItems[i].position.bottom && optionItem.position.top < this.inputItems[i].position.bottom && optionItem.position.top > this.inputItems[i].position.top) {
					/* Element inside horizontally */
					if (optionItem.position.right > this.inputItems[i].position.left && optionItem.position.right < this.inputItems[i].position.right && optionItem.position.left < this.inputItems[i].position.right && optionItem.position.left > this.inputItems[i].position.left) {
						return this.inputItems[i];
					}
				}
			}
		}
		return false;
	};

	/**
  * Propagate validation to the nested inputItems and return Object.
  * @return {Object} - Object containing an array of each InputItem and a boolean that is true if this query is valid: { allAvalid: bool, inputItems: InputItem[] }.
  */


	QueryItem.prototype.validate = function validate() {
		var validationData = {
			allValid: true,
			inputItems: []
		};

		for (var i = 0; i < this.inputWrappers.length; i++) {
			var validation = this.inputWrappers[i].validate();
			if (!validation.valid) {
				validationData.allValid = false;
			}
			validationData.inputItems.push(this.inputWrappers[i].input);
		}

		return validationData;
	};

	/**
  * Generates dragable items. Requires options to be set in QueryItem.queryData.
  * @return {(dom|false)} - A newly created optionsContainer dom element, wrapping each OptionItem or false if no options present for this QueryItem.
  */
	QueryItem.prototype.renderOptions = function renderOptions() {
		if (this.queryData.options) {
			this.optionsContainer = document.createElement("div");
			this.optionsContainer.classList.add("dt-section-container-options");

			for (var i = 0; i < this.queryData.options.length; i++) {
				var optionItem = new OptionItem(this.queryData, i).createOption();
				this.optionsContainer.appendChild(optionItem.dom);
				this.optionItems[i] = optionItem;

				/* Make option item draggable */
				this.makeOptionDraggable(optionItem);
				/* Reset option item if moved out of assigned input item upon window size chage */
				this.optionItemResetPageResizeListener(optionItem);
			}

			var optionsClearer = document.createElement("div");
			optionsClearer.classList.add("dt-options-clearer");
			this.optionsContainer.appendChild(optionsClearer);

			return this.optionsContainer;
		}
		return false;
	};

	/**
  * Resets OptionItem if moved out of assigned input item upon window size chage.
  * @param {OptionItem} optionItem - Use the passed optionItem to add an action to upon event listener call.
  * @param {function} callback - Potential callback function.
  * @return {QueryItem} - Returns instance of this QueryItem.
  */


	QueryItem.prototype.optionItemResetPageResizeListener = function optionItemResetPageResizeListener(optionItem, callback) {
		var _this = this;

		var inlineCSSbackup = optionItem.dom.getAttribute("style");
		window.addEventListener("resize", function () {
			if (optionItem.input) {
				if (!_this.checkIfOptionItemIsInsideInput(optionItem)) {
					/* Remove all data attributes on position reset */
					var attributesToRemove = [];
					for (var i = 0; i < optionItem.dom.attributes.length; i++) {
						if (optionItem.dom.attributes[i].name.indexOf("data-") === 0) {
							attributesToRemove.push(optionItem.dom.attributes[i].name);
						}
					}

					for (var _i = 0; _i < attributesToRemove.length; _i++) {
						optionItem.dom.removeAttribute(attributesToRemove[_i]);
					}

					/* Position reset */
					optionItem.dom.style = inlineCSSbackup;
					optionItem.positionChanged();
					optionItem.dom.classList.add("dt-option-reset");
				}
			}
		});
		return this;
	};

	/**
  * Sets draggable plugin for this QueryItem.
  * @param {(string|function)} [draggablePlugin] - If string passed one of the pre-set ones. If function passed thant this will be used for draggable. If ommited the default will be set.
  */


	/**
  * Makes passed OptionItem graggable.
  * Requires interact.js module!
  * @param {OptionItem} optionItem - Use the passed optionItem to make it draggable.
  * @return {QueryItem} - Returns instance of this QueryItem.
  */
	QueryItem.prototype.makeOptionDraggable = function makeOptionDraggable(optionItem) {
		var _this2 = this;

		/* Adds "option-draggable" class name to an OptionItem's dom that is about to become draggable */
		optionItem.dom.classList.add("option-draggable");
		optionItem.dom.setAttribute('draggable', true);

		CommonFormFunctions.draggableSnippet({
			item: optionItem.dom,
			container: this.queryData.sectionContainer,
			onPosChange: function onPosChange() {
				var inputItem = _this2.checkIfOptionItemIsInsideInput(optionItem);
				optionItem.positionChanged(inputItem);
			},
			onEnd: function onEnd() {
				var inputItem = _this2.checkIfOptionItemIsInsideInput(optionItem);
				optionItem.positionChanged(inputItem);
			},
			plugin: this.draggable
		});

		/* Draggable mobile fix: Prevent page scroll on drag event */
		optionItem.dom.addEventListener('touchmove', function (event) {
			event.preventDefault();
		}, false);

		return this;
	};

	/**
  * Creates InputItemWrapper and assigns it to the internal list of such items.
  * @return {QueryItem} - Returns instance of this QueryItem.
  */


	QueryItem.prototype.createInput = function createInput() {
		var newInput = new InputItemWrapper(this.queryData, this.inputItems.length).createInput();
		this.queryData.sectionContainer.appendChild(newInput.dom);
		this.inputWrappers.push(newInput);
		this.inputItems.push(newInput.input);

		return this;
	};

	/**
  * Writes the content of this QueryItem to a specified DOM container.
  * @param {dom} container - Use the passed dom element to contain this QueryItem.
  * @return {QueryItem} - Returns instance of this QueryItem.
  */


	QueryItem.prototype.write = function write(container) {
		this.container = container || this.queryData.sectionContainer;

		CommonFormFunctions.empty(this.container); // Clear section container
		CommonFormFunctions.appendStyle(this.container, this.queryData.css);

		/* Section Title */
		if (this.queryData.title) {
			var title = document.createElement("span");
			title.classList.add("dt-section-container-title");
			title.textContent = this.queryData.title;
			this.container.appendChild(title);
		}

		/* Section Subtitle */
		if (this.queryData.subtitle) {
			var subtitle = document.createElement("span");
			subtitle.classList.add("dt-section-container-subtitle");
			subtitle.textContent = this.queryData.subtitle;
			this.container.appendChild(subtitle);
		}

		/* Section Input */
		if (this.queryData.type) {
			var hasOptions = false;

			if (this.queryData.before) {
				this.container.appendChild(this.queryData.before);
			}

			var count = 0;
			do {
				this.createInput();
				count++;
			} while (count < this.queryData.items.length);

			if (this.queryData.options.length > 0) {
				if (this.queryData.optionsOrder != "before") {
					this.container.appendChild(this.renderOptions());
				} else {
					this.container.insertBefore(this.renderOptions(), this.inputWrappers[0].dom);
				}
			}

			if (this.queryData.after) {
				this.container.appendChild(this.queryData.after);
			}
		}

		this.queryData.sectionContainer = this.container;

		return this;
	};

	_createClass(QueryItem, [{
		key: "value",


		/**
   * Get all nested inputItems values.
   * @return {string[]} - An array of all InpuItem values.
   */
		get: function get() {
			var values = [];
			for (var i = 0; i < this.inputItems.length; i++) {
				values.push(this.inputItems[i].inputData);
			}
			return values;
		}
	}, {
		key: "draggable",
		set: function set(draggablePlugin) {
			draggablePlugin = draggablePlugin || "default";

			if (typeof draggablePlugin === "function") {
				this.draggablePlugin = draggablePlugin;
				return;
			}

			switch (draggablePlugin) {
				case "jQuery":
					this.draggablePlugin = CommonFormFunctions.draggablePlugin_jQuery;
					break;
				case "interact":
					this.draggablePlugin = CommonFormFunctions.draggablePlugin_interact;
					break;
				default:
					this.draggablePlugin = CommonFormFunctions.draggablePlugin_interact;
			}
		}

		/**
   * Get pre-set draggable plugin or default if none set.
   * @return {function} - A function that takes care of draggable.
   */
		,
		get: function get() {
			return this.draggablePlugin || CommonFormFunctions.draggablePlugin_interact;
		}
	}]);

	return QueryItem;
}();

/**
 * Exception block for QueryItem
 * @see {@link https://developer.mozilla.org/cs/docs/Web/JavaScript/Reference/Statements/throw|MDN Throw Statement}
 * @param {Object} message - Exception message Object.
 * @param {string} message.message - Message text.
 */


function QueryItemException(message) {
	this.message = message;
	this.name = "QueryItemException";
}"use strict";

/**
 * QueryValidator parses given restrictions on user input and manages user input comparison and validation.
 */

var _typeof = typeof Symbol === "function" && typeof Symbol.iterator === "symbol" ? function (obj) { return typeof obj; } : function (obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; };

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var QueryValidator = function () {
	/**
  * Create QueryValidator. 
  * @param {(string|boolean|RegExp|string[]|boolean[]|RegExp[])} validationData - Defines basis for user input comparison.
  */
	function QueryValidator(validationData) {
		_classCallCheck(this, QueryValidator);

		this.regExps = [];
		this.strings = [];
		this.booleans = [];
		this.mixture = [];

		this.parseValidationData(validationData);
	}

	/**
  * Chceck whether such rule has been registered.
  * @param {(string|boolean|RegExp)} rule - a rule to be searched for in the QueryValidator.
  * @return {boolean} - If a rule was found or not.
  */


	QueryValidator.prototype.hasRule = function hasRule(rule) {
		for (var i = 0; i < this.mixture.length; i++) {
			if (rule instanceof RegExp) {
				if (String(this.mixture[i]) === String(rule)) {
					return true;
				}
			} else {
				if (this.mixture[i] === rule) {
					return true;
				}
			}
		}
		return false;
	};

	/**
  * Adds a regular expression to the list of regular expressions as well as to the mixture array.
  * @param {RegExp} regExp - A regular espression to be registered for later comparison.
  * @return {QueryValidator} - This instance.
  */


	QueryValidator.prototype.appendRegExp = function appendRegExp(regExp) {
		this.regExps.push(regExp);
		this.mixture.push(regExp);
		return this;
	};

	/**
  * Adds a string to the list of strings as well as to the mixture array.
  * @param {string} string - A string to be registered for later comparison.
  * @return {QueryValidator} - This instance.
  */


	QueryValidator.prototype.appendString = function appendString(string) {
		this.strings.push(string);
		this.mixture.push(string);
		return this;
	};

	/**
  * Adds a boolean to the list of booleans as well as to the mixture array.
  * @param {boolean} bool - A boolean value to be registered for later comparison.
  * @return {QueryValidator} - This instance.
  */


	QueryValidator.prototype.appendBoolean = function appendBoolean(bool) {
		this.booleans.push(bool);
		this.mixture.push(bool);
		return this;
	};

	/**
  * Takes a single expression or an array of expressions designed for a specific QueryItem
  * and pushes them to be registered for later validation comparison.
  * @param {(string|boolean|RegExp|string[]|boolean[]|RegExp[])} validationData - Defines basis for user input comparison.
  * @return {QueryValidator} - This instance.
  */


	QueryValidator.prototype.parseValidationData = function parseValidationData(validationData) {
		if ((typeof validationData === "undefined" ? "undefined" : _typeof(validationData)) !== (typeof undefined === "undefined" ? "undefined" : _typeof(undefined)) && validationData !== null) {
			if (validationData.constructor === Array) {
				for (var i = 0; i < validationData.length; i++) {
					this.parseSingleValidationItem(validationData[i]);
				}
			} else {
				this.parseSingleValidationItem(validationData);
			}
		} else {
			this.off = true;
		}
		return this;
	};

	/**
  * Sets match type for this QueryValidator to take passed arrays as AND or OR. AND: Each input entry must have a match, OR: At least on given entry must have a match.
  * @param {string} [type] - Defines given array match type to AND or OR.
  * @return {string} - Set validation match option.
  */


	QueryValidator.prototype.setMatch = function setMatch(type) {
		type = type || "and";
		switch (type) {
			case "and":
				this.and = true;
				this.or = false;
				return "and";
			case "or":
				this.or = true;
				this.and = false;
				return "or";
			default:
				this.and = true;
				this.or = false;
				return "default";
		}
	};

	/**
  * Convert some known words into booleans
  * @param {(string|boolean)} value - A word to be potentionally converted into boolean.
  * @return {(boolean|null)} - A converted word, passed boolean or null if conversion was not possible/successful.
  */


	QueryValidator.prototype.booleanStringConversion = function booleanStringConversion(value) {
		if (value === "true" || value === "false") {
			return value === "true";
		} else if (value === "yes" || value === "no") {
			return value === "yes";
		} else if (value === true) {
			return true;
		} else if (value === false) {
			return false;
		}
		return null;
	};

	/**
  * Parses a signle validation item designed for a specific QueryItem and assigns it to the apropriete category (String, RegExp, Bool).
  * In addition allows for complex string values separed by commas - dividing them by commas and parsing each item separately.
  * @param {(string|boolean|RegExp)} itemData - Takes one element for later user input comparison.
  * @param {number} [depth=1] - Tells the method how deep in the recursion it is. Default is 1, max depth allowed is 2.
  * @return {boolean} - State of the parse operation, false on error, otherwise true.
  */


	QueryValidator.prototype.parseSingleValidationItem = function parseSingleValidationItem(itemData, depth) {
		depth = depth || 1;
		var maxDepth = 2;

		if (itemData instanceof RegExp) {
			this.appendRegExp(itemData);
		} else if (typeof itemData === "boolean") {
			this.appendBoolean(itemData);
		} else {
			var parsedData = this.parseRegExpRule(itemData);
			if (!parsedData) {
				console.error("Unknow check rule expression", parsedData);
				return false;
			} else if (parsedData instanceof RegExp) {
				this.appendRegExp(parsedData);
			} else {
				// Convert some known words into booleans
				var stringConversion = this.booleanStringConversion(parsedData);
				if (stringConversion !== null) {
					this.appendBoolean(stringConversion);
				} else {
					// Parse string ... subdivide a string if values in it are separated by comma.
					var separateValues = parsedData.split(", ");
					if (separateValues.length > 1 && depth < maxDepth) {
						for (var i = 0; i < separateValues.length; i++) {
							if (!this.parseSingleValidationItem(separateValues[i], depth++)) {
								return false;
							}
						}
					} else {
						this.appendString(parsedData);
					}
				}
			}
		}
		return true;
	};

	/**
  * Parses a given string into RegExp.
  * @param {string} rule - Takes a string which is in the form of a regular expression like this: "/^[a-zA-Z ]+$/".
  * @return {(RegExp|string|boolean)} - If it could parse the given string returns the final RegExp,
  * otherwise a given string is returned back. In case string was not passed to the method false is returned.
  */


	QueryValidator.prototype.parseRegExpRule = function parseRegExpRule(rule) {
		if (typeof rule === "string" || rule instanceof String) {
			try {
				// Source: http://stackoverflow.com/a/874742
				var flags = rule.replace(/.*\/([gimyu]*)$/, "$1");
				var pattern = rule.replace(new RegExp("^/(.*?)/" + flags + "$"), "$1");
				return new RegExp(pattern, flags);
			} catch (e) {
				return rule;
			}
		}
		return false;
	};

	/**
  * Compares two given values, where the first given value is universal for String and RegExp - won't work with Booleans.
  * If a RegExp is passed the comparison is done in the form of a RegExp test on the second value.
  * @param {(string|RegExp)} first - A first value for comparison.
  * @param {string} second - A second value for comparison.
  * @return {boolean} - Returns whether the two given values equal or not.
  */


	QueryValidator.prototype.compareValues = function compareValues(first, second) {
		if ((typeof first === "undefined" ? "undefined" : _typeof(first)) !== (typeof undefined === "undefined" ? "undefined" : _typeof(undefined)) && second !== (typeof undefined === "undefined" ? "undefined" : _typeof(undefined))) {
			if (typeof second === "string" || second instanceof String) {
				if (first instanceof RegExp) {
					return first.test(second);
				}
				if (typeof first === "string" || first instanceof String) {
					return first === second;
				}
			}
		}
		return false;
	};

	/**
  * Compares given value only to the registered booleans.
  * @param {boolean} value - A boolean value for comparison.
  * @param {number} [index] - If index is given the value is compared only with an item on the specified index.
  * @return {boolean} - Returns whether the given values mathes any of the registered boolean rules or not.
  */


	QueryValidator.prototype.testValueToAllBooleans = function testValueToAllBooleans(value, index) {
		if ((typeof index === "undefined" ? "undefined" : _typeof(index)) !== undefined && !isNaN(index) && index >= 0) {
			return this.booleans.length > index && value === this.booleans[index];
		}

		var atLeastOneCorrect = false;
		if ((typeof value === "undefined" ? "undefined" : _typeof(value)) !== (typeof undefined === "undefined" ? "undefined" : _typeof(undefined))) {
			for (var i = 0; i < this.booleans.length; i++) {
				if (value === this.booleans[i]) {
					atLeastOneCorrect = true;
				}
			}
		}
		return atLeastOneCorrect;
	};

	/**
  * Compares given value only to the registered strings.
  * @param {string} value - A string value for comparison.
  * @param {number} [index] - If index is given the value is compared only with an item on the specified index.
  * @return {boolean} - Returns whether the given values mathes any of the registered string rules or not.
  */


	QueryValidator.prototype.testValueToAllStrings = function testValueToAllStrings(value, index) {
		if ((typeof index === "undefined" ? "undefined" : _typeof(index)) !== undefined && !isNaN(index) && index >= 0) {
			return this.strings.length > index && value === this.strings[index];
		}

		var atLeastOneCorrect = false;
		if ((typeof value === "undefined" ? "undefined" : _typeof(value)) !== (typeof undefined === "undefined" ? "undefined" : _typeof(undefined))) {
			for (var i = 0; i < this.strings.length; i++) {
				if (this.strings[i] === value) {
					atLeastOneCorrect = true;
				}
			}
		}
		return atLeastOneCorrect;
	};

	/**
  * Compares given value only to the registered RegExps.
  * @param {RegExp} value - A RegExp value for comparison.
  * @param {number} [index] - If index is given the value is compared only with an item on the specified index.
  * @return {boolean} - Returns whether the given values mathes any of the registered RegExp rules or not.
  */


	QueryValidator.prototype.testValueToAllRegExps = function testValueToAllRegExps(value, index) {
		if ((typeof index === "undefined" ? "undefined" : _typeof(index)) !== undefined && !isNaN(index) && index >= 0) {
			return this.regExps.length > index && this.regExps[index].test(value);
		}

		var atLeastOneCorrect = false;
		if ((typeof value === "undefined" ? "undefined" : _typeof(value)) !== (typeof undefined === "undefined" ? "undefined" : _typeof(undefined))) {
			for (var i = 0; i < this.regExps.length; i++) {
				if (this.regExps[i].test(value)) {
					atLeastOneCorrect = true;
				}
			}
		}
		return atLeastOneCorrect;
	};

	/**
  * Get the itemToValidate value.
  * @return {InputItem} - The itemToValidate value.
  */


	/**
  * Validates any mixture content (array of strings or booleans and any mixture of those) to all inner values.
  * @param {(boolean[]|string[]|mixed)} values - The itemToValidate value.
  * @return {boolean} - Return true if all values match (AND) or at least one matches (OR).
  */
	QueryValidator.prototype.validateAnyMixture = function validateAnyMixture(values) {
		values = values || [];
		var valid = false;

		// If each must have a match and the number of present rules is not the same as the number of input values than this is invalid right away.
		if (this.and && values.length !== this.mixture.length) {
			valid = false;
		} else {
			for (var i = 0; i < values.length; i++) {
				var stringConversion = this.booleanStringConversion(values[i]);
				if (stringConversion !== null && typeof stringConversion === "boolean") {
					// Test to all booleans
					valid = this.testValueToAllBooleans(stringConversion);
				} else {
					if (this.strings.length > 0) {
						// Test to all strings
						valid = this.testValueToAllStrings(values[i]);
					}
					if (!valid && this.regExps.length > 0) {
						// Test to all RegExps
						valid = this.testValueToAllRegExps(values[i]);
					}
				}

				if (this.and && valid === false) {
					break;
				}
			}
		}
		return valid;
	};

	/**
  * Validates any mixture content (array of strings or booleans and any mixture of those) to all inner values in rule order.
  * @param {(boolean[]|string[]|mixed)} values - The itemToValidate value.
  * @return {boolean} - Return true if all values match (AND) or at least one matches (OR).
  */


	QueryValidator.prototype.validateAnyMixtureInRuleOrder = function validateAnyMixtureInRuleOrder(values) {
		values = values || [];
		var valid = false;

		if (values.length !== this.mixture.length) {
			valid = false;
		} else {
			for (var i = 0; i < values.length; i++) {
				var stringConversion = this.booleanStringConversion(values[i]);
				if (stringConversion !== null && typeof stringConversion === "boolean") {
					// Test on booleans
					valid = this.mixture[i] === stringConversion;
				} else {
					// Test on strings and RegExps
					valid = this.compareValues(this.mixture[i], values[i]);
				}
				if (!valid) {
					break;
				}
			}
		}
		return valid;
	};

	/**
  * Validates a preset itemToValidate (InputItem) using the registered rules.
  * @param {(boolean[]|string[]|mixed)} [values] - An optional array of booleans, strings or mixed parameter overwiting a value given by InputItem.
  * @return {boolean} - Returns whether the assigned InputItem matches the preset rules.
  */


	QueryValidator.prototype.validate = function validate(values) {
		if (this.off) {
			// If Validation is not requested for this item always return true;
			return true;
		}

		// True if the default input item value should be overwritten.
		var overwrite = (typeof values === "undefined" ? "undefined" : _typeof(values)) === (typeof undefined === "undefined" ? "undefined" : _typeof(undefined)) ? false : true;

		// If there is no overwrite value to validate set and input item to be validated present.
		var valid = true;

		// Validate specifix input items - IGNORES CUSTOM VALUES, THOUGH THEY ARE TAKEN CARE IN A DIFFERENT SECTION.
		if (this.itemToValidate) {
			// Validate input type checkbox and radio
			// In case more booleans registered here at least one has to match - Works as OR because AND would not make sense for boolean values.
			if (this.itemToValidate.queryData.type === "radio" || this.itemToValidate.queryData.type === "checkbox") {
				valid = this.testValueToAllBooleans(this.itemToValidate.dom.checked);
			}
			// Validate input type order: Will keep order for items validation.
			// The only way to make this a bit variable is to use RegExp on relevant option match.
			else if (this.itemToValidate.queryData.type === "order") {
					var items = this.itemToValidate.value;
					valid = this.validateAnyMixtureInRuleOrder(items);
				} else {
					valid = this.validateAnyMixture(this.itemToValidate.value);
				}
		} else {
			// Validation any value
			valid = this.validateAnyMixture(overwrite ? values : this.itemToValidate.value);
		}
		this.lastValidationResult = valid;
		return valid;
	};

	_createClass(QueryValidator, [{
		key: "itemToValidate",
		get: function get() {
			return this.item || null;
		}

		/**
   * Set the itemToValidate value.
   * @param {InputItem} item - The itemToValidate value.
   */
		,
		set: function set(item) {
			this.item = item || null;
		}
	}]);

	return QueryValidator;
}();