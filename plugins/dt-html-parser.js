"use strict";

/**
 * A DT library plugin to convert a simple HTML structure to a DT enhanced test form.
 */
(function(window, document) {
	function init() {
		let formsholder = new FormsHolder();
		
		let domForms = document.getElementsByTagName("form");
		let forms = [];
		for(let i = 0; i < domForms.length; i++) {
			if(domForms[i].dataset.dt !== "yes") {
				break;
			}
			
			let inputs = domForms[i].getElementsByTagName("input");
			let submitBtn = null;
			for(let j = 0; j < inputs.length; j++) {
				if(inputs[j].type == "submit") {
					submitBtn = inputs[j];
				}
			}

			forms[i] = formsholder.createForm({
				baseForm: domForms[i],
				allowCorrections: domForms[i].dataset.dtallowcorrections ? domForms[i].dataset.dtallowcorrections === "true" : false,
				customSubmit: domForms[i].dataset.ctcustomsubmit || false,
				scrollToError: domForms[i].dataset.dtscrolltoerror || false,
				type: domForms[i].dataset.dttype || "",
				submitBtn: submitBtn,
				submitUrl: domForms[i].dataset.submitUrl || null,
				draggable: domForms[i].dataset.dtplugin || null
			});
			parseForm(forms[i]);
		}
	}

	function parseForm(form) {
		let dataWrappers = form.baseForm.getElementsByTagName("dtwrapper");				
		for(let i = 0; i < dataWrappers.length; i++) {
			switch(dataWrappers[i].dataset.dttype) {
				case "text":
					parseText(form, dataWrappers[i], i);
					break;
				case "assign":
					parseAssign(form, dataWrappers[i], i);
					break;
				case "order":
					parseOrder(form, dataWrappers[i], i);
					break;
				case "radio":
					parseRadio(form, dataWrappers[i], i);
					break;
				case "checkbox":
					parseCheckbox(form, dataWrappers[i], i);
					break;
				default:
					console.error("Unknown type");
			}
		}
	}

	function getElementStyle(element) {
		return String(element.style.cssText);
	}

	function reorderItems(items) {
		if(items && items.length > 1 && items[0].order) {
			items = Array.from(items).sort(function(a, b) {
				return parseInt(a.order || 0) - parseInt(b.order || 0);
			});
		}
		return items;
	}

	function parseGeneral(domItems, id, customStructure) {
		domItems = domItems || [];
		let items = [];
		
		for(let i = 0; i < domItems.length; i++) {
			id = id || domItems[i].id || "";
						
			if(customStructure) {
				items.push(customStructure(domItems[i], i, id));
			}
			else {
				items.push({
					label: domItems[i].dataset.dtlabel || "",
					id: domItems[i].value || CommonFormFunctions.joinStrings(id, i, "-"),
					text: domItems[i].value,
					checkRules: domItems[i].dataset.dtcheckrules || null,
					arrayMatch: domItems[i].dataset.dtarraymatch || null,
					error: domItems[i].dataset.dterror || null,
					required: domItems[i].hasAttribute("required") ? true : false,
					css: getElementStyle(domItems[i]),
					order: domItems[i].dataset.order || ""
				});
			}
		}
		
		while(domItems.length > 0) {
			let item = domItems[domItems.length - 1];
			item.parentNode.removeChild(item);
		}
		return reorderItems(items);
	}
	
	function getQueryTitle(element) {
		/* Check if custom query title is set */
		if(element.dataset.title) {
			return element.dataset.title;
		}
		/* Else: Check for previous siblings to get the title name from. Delete the tag if present. */
		let previousSibling = element.previousElementSibling;
		if(previousSibling) {
			/* If tag is a header */
			if(/h[1-6]/.test(previousSibling.tagName.toLowerCase())) {
				let title = previousSibling.textContent;
				previousSibling.parentElement.removeChild(previousSibling);
				return title;
			}
		}
		/* Return null if nothing present */
		return null;
	}
	
	/* Check whether any of the items is required */
	function anyItemRequired(items) {
		for(let i = 0; i < items.length; i++) {
			if(items[i].required === true) {
				return true;
			}
		}
		return false;
	}

	function parseText(form, element, index) {
		let items = parseGeneral(element.getElementsByTagName("input"));
		
		if(items.length === 1) {
			let query = form.newQueryItem({
				id: items[0].id,
				title: getQueryTitle(element),
				subtitle: element.dataset.dtsubtitle || null,
				type: "text",
				label: items[0].label,
				checkRules: items[0].checkRules,
				error: element.dataset.dterror || null,
				required: items[0].required,
				css: getElementStyle(element)
			}, true);
			element.appendChild(query.container);
		}
	}

	function parseAssign(form, element, index) {
		let inputs = parseGeneral(element.getElementsByTagName("input"), "assign-item");
		let selects = element.getElementsByTagName("select");
		if(inputs.length >= 1 && selects.length === 1) {					
			let select = selects[0];
			let options = parseGeneral(select.getElementsByTagName("option"), "draggable-item", function(option) {
				return {
					id: option.value || "",
					label: option.innerHTML,
					text: option.innerHTML,
					css: getElementStyle(option)
				};
			});
			select.parentNode.removeChild(select);
			let query = form.newQueryItem({
				id: "assign",
				title: getQueryTitle(element),
				subtitle: element.dataset.dtsubtitle || null,
				type: "assign",
				items: inputs,
				options: options,
				error: element.dataset.dterror || null,
				optionsOrder: element.dataset.dtoptionsorder || null,
				css: getElementStyle(element)
			}, true);
			element.appendChild(query.container);
		}
		else {
			console.error("Exactly one select element must be present in one dtwrapper, " + selects.length + " present");
		}
	}

	function parseOrder(form, element, index) {
		let inputs = element.getElementsByTagName("input");
		let selects = element.getElementsByTagName("select");
		if(inputs.length <= 1 && selects.length === 1) {
			let input = inputs.length === 1 ? inputs[0] : null;
			let inputDirection = input && input.dataset.dtdirection ? input.dataset.dtdirection : "vertical";
			let inputCheckRules = input && input.dataset.dtcheckrules ? input.dataset.dtcheckrules : null;
			let inputOptionsOrder = input && input.dataset.dtoptionsorder ? input.dataset.dtoptionsorder : null;
			input.parentNode.removeChild(input);
			
			let select = selects[0];
			let options = parseGeneral(select.getElementsByTagName("option"), "draggable-item", function(option) {
				return {
					id: option.value,
					label: option.innerHTML,
					text: option.innerHTML,
					css: getElementStyle(option),
					order: option.dataset.order || ""
				};
			});
			select.parentNode.removeChild(select);
			let query = form.newQueryItem({
				id: "draggable-" + inputDirection,
				title: getQueryTitle(element),
				subtitle: element.dataset.dtsubtitle || null,
				type: "order",
				direction: inputDirection,
				checkRules: inputCheckRules,
				error: element.dataset.dterror || null,
				options: options,
				optionsOrder: inputOptionsOrder,
				css: getElementStyle(element)
			}, true);
			element.appendChild(query.container);	
		}
		else {
			console.error("Exactly one select element must be present in one dtwrapper, " + selects.length + " present");
		}
	}	

	function parseRadio(form, element, index) {
		let items = parseGeneral(element.getElementsByTagName("input"), "radio-item");
		let query = form.newQueryItem({
			id: "radio-" + index,
			title: getQueryTitle(element),
			subtitle: element.dataset.dtsubtitle || null,
			type: "radio",
			required: anyItemRequired(items),
			error: element.dataset.dterror || null,
			items: items,
			css: getElementStyle(element)
		}, true);
		element.appendChild(query.container);
	}

	function parseCheckbox(form, element, index) {
		let items = parseGeneral(element.getElementsByTagName("input"), "checkbox-item");
		let query = form.newQueryItem({
			id: "checkbox-" + index,
			title: getQueryTitle(element),
			subtitle: element.dataset.dtsubtitle || null,
			type: "checkbox",
			required: anyItemRequired(items),
			error: element.dataset.dterror || null,
			items: items,
			css: getElementStyle(element)
		}, true);
		element.appendChild(query.container);
	}
	
	window.addEventListener("load", (event) => {
		init();
	});
})(window, document);
