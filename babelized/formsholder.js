"use strict";

/**
 * This class holds separate Form instances on one page.
 */

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var FormsHolder = function () {
	/**
  * Create FormsHolder.
  */
	function FormsHolder() {
		_classCallCheck(this, FormsHolder);

		this.forms = [];
	}

	/**
  * Creates a new Form object based on passed data.
  * @see {@link Form} for more information on the Object structure.
  * @param {Object} data - An object defining the properties of a Form.
  * @return {Form} - The new Form object created.
  */


	FormsHolder.prototype.createForm = function createForm(data) {
		data = data || {};
		var newForm = new Form(data, this.forms.length);
		this.addForm(newForm);
		return newForm;
	};

	/**
  * Loggs a form to the internal array.
  * @param {Form} form - A Form to be logged in an internal structure.
  * @return {boolean} - Return true if registering the Form was successful or false if it was already registered prior to this attempt.
  */


	FormsHolder.prototype.addForm = function addForm(form) {
		if (!this.isAlreadyLogged(form)) {
			this.forms.push(form);
			return true;
		}
		return false;
	};

	/**
  * Removes a form from the internal array.
  * @param {Form} form - A Form to be removed from the internal structure.
  * @return {boolean} - Return true if unregistering the Form was successful or false if it was already unregistered prior to this attempt.
  */


	FormsHolder.prototype.removeForm = function removeForm(form) {
		var index = this.formIndex(form);
		if (index >= 0) {
			this.forms.split(index, 1);
			return true;
		}
		return false;
	};

	/**
  * Determines whether the form is already logged or not
  * @param {Form} form - A Form to be found in the internal structure.
  * @return {bool} - If true the Form is registered in the structure. If false it is not.
  */


	FormsHolder.prototype.isAlreadyLogged = function isAlreadyLogged(form) {
		return this.formIndex(form) >= 0;
	};

	/**
  * Checks for the Form index in the internal structure.
  * @param {Form} form - A Form to be found in the internal structure.
  * @return {number} - Form index position, -1 means it is not present and numbers greater or equal 0 means the form is already present
  */


	FormsHolder.prototype.formIndex = function formIndex(form) {
		return this.forms.indexOf(form);
	};

	return FormsHolder;
}();