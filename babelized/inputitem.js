"use strict";

/**
 * InputItem holds input DOM and takes care of its validation using QueryValidator.
 */

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _defaults(obj, defaults) { var keys = Object.getOwnPropertyNames(defaults); for (var i = 0; i < keys.length; i++) { var key = keys[i]; var value = Object.getOwnPropertyDescriptor(defaults, key); if (value && value.configurable && obj[key] === undefined) { Object.defineProperty(obj, key, value); } } return obj; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : _defaults(subClass, superClass); }

var InputItem = function (_CommonFormMethods) {
	_inherits(InputItem, _CommonFormMethods);

	/**
  * Create InputItemWrapper. 
  * @param {QueryDataBlock} queryData - An instance of QueryData.
  * @param {number} index - Order index of the input item in a query.
  */
	function InputItem(queryData, index) {
		var _ret;

		_classCallCheck(this, InputItem);

		if (!queryData) {
			throw new InputItemException("No data passed when creating a new instance of InputItemException");
		}
		if (!queryData instanceof QueryDataBlock) {
			throw new InputItemException("Data passed when creating new instance of InputItemException were not an instance of a QueryDataBlock");
		}

		var _this = _possibleConstructorReturn(this, _CommonFormMethods.call(this));

		_this.queryData = queryData;
		_this.index = index;

		_this.internalValue = {
			assignedOptions: [],
			text: "",
			checked: false
		};

		_this.valid = true;
		_this.associateCheckRule();

		return _ret = _this, _possibleConstructorReturn(_this, _ret);
	}

	/**
  * Creates InputItem DOM representation.
  * @return {InputItem} - This instance.
  */


	InputItem.prototype.createInput = function createInput() {
		this.dom = document.createElement("input");
		this.dom.classList.add("dt-section-container-input", "dt-section-container-input-type-" + this.queryData.type);
		if (this.queryData.direction) {
			this.dom.classList.add(this.queryData.direction);
		}
		this.dom.setAttribute("type", this.queryData.inputType);
		this.dom.setAttribute("id", this.htmlId);
		this.dom.setAttribute("name", this.name);

		if (this.queryData.type) {
			if (this.queryData.type === "order" || this.queryData.type === "assign") {
				this.restrictWriteAccess = true;
			}
		}

		if (!this.standsAlone) {
			if (this.queryData.properInputType) {
				if (this.queryData.properInputType === "radio" || this.queryData.properInputType === "checkbox") {
					this.dom.setAttribute("value", this.currentSubItem.id);
				}
			}
			CommonFormFunctions.appendStyle(this.dom, this.currentSubItem.css);
		}

		if (this.queryData.required) {
			this.dom.setAttribute("required", "");
		}

		return this;
	};

	/**
  * Associtate this InputItem with QueryValidator for its future validation.
  * Directly refferences to the QueryDataBlock's QueryValidator for this item.
  * @return {InputItem} - This instance.
  */


	InputItem.prototype.associateCheckRule = function associateCheckRule() {
		this.currentSubItem.queryValidator.itemToValidate = this;
		return this;
	};

	/**
  * Set if this InputItem's dom input is user editable.
  * @param {boolean} readOnly - Of true the input item won't be user editable - usable eg. on value assign queries.
  */


	/**
  * Validate data in this InputItem's DOM input = using adequate QueryValidator.
  * @return {InputItem} - This instance.
  */
	InputItem.prototype.validate = function validate() {
		if (this.dom) {
			this.valid = this.currentSubItem.queryValidator.validate();
			this.setValid();
		}
		return this.valid;
	};

	/**
  * Get all input item values, either the one typed in or all the assigned options.
  * @return {string[]} - An array containing all assigned values or a single inserted text value.
  */


	/**
  * Remove OptionItem from this instance of InputItem.
  * @return {InputItem} - This instance.
  */
	InputItem.prototype.deleteOption = function deleteOption(option) {
		var optionIndex = this.internalValue.assignedOptions.indexOf(option);
		if (optionIndex > -1) {
			this.internalValue.assignedOptions.splice(optionIndex, 1);
		}
		return this;
	};

	/**
  * Order OptionItems in InputItem by their position rather than the order they were dragged in (reorder).
  * Depends on Query direction value - "vertical" or "horizontal" - if no match found fallbacks to "horizontal".
  * @return {OptionItem[]} - Ordered Option items.
  */


	InputItem.prototype.orderAssignedOptions = function orderAssignedOptions() {
		for (var i = 0; i < this.internalValue.assignedOptions.length; i++) {
			this.internalValue.assignedOptions[i].updatePosition();
		}

		if (this.internalValue.assignedOptions && this.internalValue.assignedOptions.length > 1 && this.queryData.direction) {
			if (this.queryData.direction === "horizontal") {
				this.internalValue.assignedOptions.sort(function (a, b) {
					return a.position.left - b.position.left;
				});
			} else {
				this.internalValue.assignedOptions.sort(function (a, b) {
					return a.position.top - b.position.top;
				});
			}
		}

		return this.internalValue.assignedOptions;
	};

	/**
  * Modify InputItem DOM input value based on current this.internalValue.assignedOptions state.
  * @return {InputItem} - This instance.
  */


	InputItem.prototype.changeAssignedOptionsValue = function changeAssignedOptionsValue() {
		var finalValue = "";
		if (this.internalValue.assignedOptions && this.internalValue.assignedOptions.length > 0) {
			var ordered = this.orderAssignedOptions();
			for (var i = 0; i < ordered.length; i++) {
				finalValue += (i > 0 ? "&" : "") + ordered[i].scope.id;
			}
		}
		this.dom.value = finalValue;
		return this;
	};

	/**
  * Adds attribute with a value to InputItem's DOM input, like: <tagname attribute="value">.
  * @param {string} attribute - The name of the attribute.
  * @param {string} value - The value of the attribute.
  * @return {InputItem} - This instance.
  */


	InputItem.prototype.setAttribute = function setAttribute(attribute, value) {
		if (attribute != null && value != null) {
			this.dom.setAttribute(attribute, value);
		}
		return this;
	};

	/**
  * Changes this InputItem DOM input class based on the validity.
  * @param {bollean} [overwriteGraphicsTo] - If set to true, the InputItem will be force-set to valid.
  * @return {InputItem} - This instance.
  */
	InputItem.prototype.setValid = function setValid(overwriteGraphicsTo) {
		if (this.valid || overwriteGraphicsTo == true) {
			this.dom.classList.remove("invalid");
		} else {
			this.dom.classList.add("invalid");
		}
		return this;
	};

	/**
  * InputItem ValueChange event - call function on value change.
  * @param {function} callback - The function that should be executed on input's value change.
  * @return {InputItem} - This instance.
  */


	InputItem.prototype.onInputValueChange = function onInputValueChange(callback) {
		var _this2 = this;

		this.dom.addEventListener("change", function () {
			callback();
			_this2.setValid(true);
		});
		return this;
	};

	_createClass(InputItem, [{
		key: "restrictWriteAccess",
		set: function set(readOnly) {
			var readOnly = readOnly || false;
			this.dom.readOnly = readOnly;
		}
	}, {
		key: "value",
		get: function get() {
			if (this.internalValue.assignedOptions && this.internalValue.assignedOptions.length > 0) {
				this.orderAssignedOptions();

				var optionIds = [];
				for (var i = 0; i < this.internalValue.assignedOptions.length; i++) {
					optionIds.push(this.internalValue.assignedOptions[i].scope.validatorMatch);
				}
				return optionIds;
			}
			this.internalValue.text = this.dom.value;
			this.internalValue.checked = this.dom.checked;
			return this.internalValue.text ? [this.internalValue.text] : [];
		}

		/**
  * Returns obect with detailed information about this InputItem.
  * @return {Object} - {name:string - input item's name, label:string - input item's label or name if no label defined, values:string[], internalValue:string[] - assigned option's text, valid: boolean}
  */

	}, {
		key: "inputData",
		get: function get() {
			var options = [];
			for (var i = 0; i < this.internalValue.assignedOptions.length; i++) {
				options.push(this.internalValue.assignedOptions[i].scope.text);
			}
			return {
				name: this.name,
				label: this.currentSubItem.label || this.name,
				values: this.value,
				internalValue: options,
				valid: this.valid,
				checked: this.dom.checked
			};
		}

		/**
   * Assign OptionItem to this instance of InputItem.
   * @param {OptionItem} option - The OptionItem instance.
   */

	}, {
		key: "option",
		set: function set(option) {
			var optionIndex = this.internalValue.assignedOptions.indexOf(option);
			if (optionIndex === -1) {
				this.internalValue.assignedOptions.push(option);
			}
		}
	}]);

	return InputItem;
}(CommonFormMethods);

/**
 * Exception block for InputItem
 * @see {@link https://developer.mozilla.org/cs/docs/Web/JavaScript/Reference/Statements/throw|MDN Throw Statement}
 * @param {Object} message - Exception message Object.
 * @param {string} message.message - Message text.
 */


function InputItemException(message) {
	this.message = message;
	this.name = "InputItem";
}