"use strict";

/**
 * OptionItem takes care of Draggable option elements on page.
 * It isn't responsible for the dragging functionality itself, but takses care of actions that are related to it.
 */

var _typeof = typeof Symbol === "function" && typeof Symbol.iterator === "symbol" ? function (obj) { return typeof obj; } : function (obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; };

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _defaults(obj, defaults) { var keys = Object.getOwnPropertyNames(defaults); for (var i = 0; i < keys.length; i++) { var key = keys[i]; var value = Object.getOwnPropertyDescriptor(defaults, key); if (value && value.configurable && obj[key] === undefined) { Object.defineProperty(obj, key, value); } } return obj; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : _defaults(subClass, superClass); }

var OptionItem = function (_CommonFormMethods) {
	_inherits(OptionItem, _CommonFormMethods);

	/**
  * Create OptionItem. 
  * @param {QueryDataBlock} queryData - An instance of QueryData.
  * @param {number} index - Order index of the option item in a query.
  */
	function OptionItem(queryData, index) {
		var _ret;

		_classCallCheck(this, OptionItem);

		if (!queryData) {
			throw new OptionItemException("No data passed when creating a new instance of OptionItem");
		}
		if (!queryData instanceof QueryDataBlock) {
			throw new OptionItemException("Data passed when creating new instance of OptionItem were not an instance of a QueryDataBlock");
		}

		var _this = _possibleConstructorReturn(this, _CommonFormMethods.call(this));

		_this.queryData = queryData;
		_this.inputItem = null;
		_this.index = index;

		return _ret = _this, _possibleConstructorReturn(_this, _ret);
	}

	/**
  * Gets inputItem.
  * @return {InputItem} - An InputItem object instance related to this OptionItem.
  */


	/**
  * Creates a dom representation of this OptionItem.
  * @return {OptionItem} - This instance.
  */
	OptionItem.prototype.createOption = function createOption() {
		this.dom = document.createElement("div");
		this.dom.classList.add("dt-section-container-option-" + String(this.index));
		this.dom.setAttribute("id", this.scope.htmlId);
		this.dom.textContent = this.scope.text;
		CommonFormFunctions.appendStyle(this.dom, this.scope.css);

		return this;
	};

	/**
  * Removes assigned InputItem from an OptionItem instance.
  * Eg. if this option item has been moved outside of the dom input element.
  * @return {OptionItem} - This instance.
  */


	OptionItem.prototype.deleteInput = function deleteInput() {
		if (this.input) {
			var optionIndex = this.inputItem.internalValue.assignedOptions.indexOf(this);
			this.inputItem.deleteOption(this);
			this.inputItem = null;

			this.dom.classList.add("is-out-of-container");
			this.dom.classList.remove("is-inside-container");
			return true;
		}
		return false;
	};

	/**
  * Supposed to be called on OptionItem position change (user drag) and calls the other necessary methods.
  * @param {InputItem} [inputItem] - If set to the instance of InputItem it will be assigned to this QueryItem an registered as "beign in its scope".
  * @return {OptionItem} - This instance.
  */


	OptionItem.prototype.positionChanged = function positionChanged(inputItem) {
		this.dom.classList.remove("dt-option-reset");

		// Backup revious input before modifying it.
		var originalInput = this.input || null;

		if ((typeof inputItem === "undefined" ? "undefined" : _typeof(inputItem)) !== (typeof undefined === "undefined" ? "undefined" : _typeof(undefined)) && inputItem instanceof InputItem) {
			this.input = inputItem;
			inputItem.changeAssignedOptionsValue();
		}

		if (originalInput !== inputItem && originalInput instanceof InputItem) {
			originalInput.deleteOption(this);
			originalInput.changeAssignedOptionsValue();
		}

		if (!inputItem) {
			this.deleteInput();
		}

		return this;
	};

	_createClass(OptionItem, [{
		key: "input",
		get: function get() {
			return this.inputItem;
		}

		/**
   * Gets data Object for this OptionItem.
   * @return {Object} - This option data from QueryDataBlock instance.
   */
		,


		/**
   * Sets InputItem to be related with this instance of OptionItem.
   * Eg. if this option item has been moved over the dom input element.
   * @param {InputItem} inputItem - The InputItem instance.
   */
		set: function set(inputItem) {
			this.inputItem = inputItem;
			this.inputItem.option = this;

			this.dom.classList.add("is-inside-container");
			this.dom.classList.remove("is-out-of-container");
		}
	}, {
		key: "scope",
		get: function get() {
			return this.queryData.options[this.index];
		}
	}]);

	return OptionItem;
}(CommonFormMethods);

/**
 * Exception block for OptionItem
 * @see {@link https://developer.mozilla.org/cs/docs/Web/JavaScript/Reference/Statements/throw|MDN Throw Statement}
 * @param {Object} message - Exception message Object.
 * @param {string} message.message - Message text.
 */


function OptionItemException(message) {
	this.message = message;
	this.name = "OptionItem";
}