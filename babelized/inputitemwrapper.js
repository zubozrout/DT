"use strict";

/**
 * InputItemWrapper wraps InputItem to take care of displaying input labels and possible errors.
 * It isn't responsible for the dragging functionality itself, but takses care of actions that are related to it.
 */

function _defaults(obj, defaults) { var keys = Object.getOwnPropertyNames(defaults); for (var i = 0; i < keys.length; i++) { var key = keys[i]; var value = Object.getOwnPropertyDescriptor(defaults, key); if (value && value.configurable && obj[key] === undefined) { Object.defineProperty(obj, key, value); } } return obj; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : _defaults(subClass, superClass); }

var InputItemWrapper = function (_CommonFormMethods) {
	_inherits(InputItemWrapper, _CommonFormMethods);

	/**
  * Create InputItemWrapper. 
  * @param {QueryDataBlock} queryData - An instance of QueryData.
  * @param {number} index - Order index of the input item in a query.
  */
	function InputItemWrapper(queryData, index) {
		_classCallCheck(this, InputItemWrapper);

		if (!queryData) {
			throw new InputItemWrapperException("No data passed when creating a new instance of InputItemWrapper");
		}
		if (!queryData instanceof QueryDataBlock) {
			throw new InputItemWrapperException("Data passed when creating new instance of InputItemWrapper were not an instance of a QueryDataBlock");
		}

		var _this = _possibleConstructorReturn(this, _CommonFormMethods.call(this));

		_this.queryData = queryData;
		_this.index = !isNaN(index) ? index : null;
		_this.input = null;
		_this.errorTag = null;

		_this.valid = true;
		return _this;
	}

	/**
  * Creates InputItem and assigns it into this InputItemWrapper.
  * @return {InputItemWrapper} - This instance.
  */


	InputItemWrapper.prototype.createInput = function createInput() {
		this.dom = document.createElement("div");
		this.dom.classList.add("dt-section-container-wrapper");
		this.createLabel();

		this.input = new InputItem(this.queryData, this.index).createInput();
		this.input.validation = this.checkRule;
		this.dom.appendChild(this.input.dom);
		this.createErrorTag();

		return this;
	};

	/**
  * Creates DOM label for this InputItemWrapper.
  * @return {InputItemWrapper} - This instance.
  */


	InputItemWrapper.prototype.createLabel = function createLabel() {
		var containerLabel = this.queryData.label;
		var itemLabel = this.queryData.items.length > 0 && this.index != null;

		if (containerLabel || itemLabel) {
			var labelTag = document.createElement("label");

			if (containerLabel) {
				labelTag.classList.add("dt-section-container-label", this.queryData.type);
				labelTag.textContent = this.queryData.label;
			}
			if (itemLabel) {
				labelTag.textContent = this.queryData.items[this.index].label;
			}

			labelTag.setAttribute("for", this.htmlId);
			this.dom.appendChild(labelTag);
		}

		return this;
	};

	/*  */
	/**
  * Creates Input Error Message DOM.
  * @return {InputItemWrapper} - This instance.
  */


	InputItemWrapper.prototype.createErrorTag = function createErrorTag() {
		var _this2 = this;

		if (this.queryData.error) {
			this.errorTag = document.createElement("span");
			this.errorTag.classList.add("dt-section-container-error");
			this.dom.appendChild(this.errorTag);

			this.input.onInputValueChange(function () {
				_this2.setValid(true);
			});
		}

		return this;
	};

	/**
  * Validates this InputItemWrapper, requests InputItem validation.
  * @return {(Object|false)} - Data validation summary from the assigned InputItem {valid:boolean, data:inputData, inputWrapper:this, inputItem:InputItem} or false if none assigned.
  */


	InputItemWrapper.prototype.validate = function validate() {
		if (this.input) {
			var valid = this.input.validate();
			this.setValid();

			return {
				valid: valid,
				data: this.input.inputData,
				inputWrapper: this,
				inputItem: this.input
			};
		}
		return false;
	};

	/**
  * Displays validation state using the errorTag, setting or unsetting classnames for them to by styllable.
  * @param {boolean} [overwriteGraphicsTo] - If set to true, the InputItemWrapper will be force-set to valid.
  * @return {OptionItem} - This instance.
  */


	InputItemWrapper.prototype.setValid = function setValid(overwriteGraphicsTo) {
		if (this.input.valid || overwriteGraphicsTo == true) {
			if (this.queryData.error && this.errorTag) {
				this.errorTag.innerText = "";
			}
			this.dom.classList.remove("invalid");
		} else {
			if (this.errorTag) {
				this.errorTag.innerText = this.currentSubItem.error || this.queryData.error || "Bad content";
			}
			this.dom.classList.add("invalid");
		}

		return this;
	};

	return InputItemWrapper;
}(CommonFormMethods);

/**
 * Exception block for InputItemWrapper
 * @see {@link https://developer.mozilla.org/cs/docs/Web/JavaScript/Reference/Statements/throw|MDN Throw Statement}
 * @param {Object} message - Exception message Object.
 * @param {string} message.message - Message text.
 */


function InputItemWrapperException(message) {
	this.message = message;
	this.name = "InputItemWrapper";
}