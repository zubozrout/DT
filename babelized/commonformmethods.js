"use strict";

/**
 * An abstract class containing some suited functionality widely used in other spedific DT classes.
 * @abstract
 */

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var CommonFormMethods = function () {
	/**
  * Create CommonFormMethods.
  */
	function CommonFormMethods() {
		_classCallCheck(this, CommonFormMethods);

		/* http://stackoverflow.com/a/30560792 */
		if (new.target === CommonFormMethods) {
			throw new TypeError("Cannot construct CommonFormMethods instance directly, abstract class");
		}
		return this;
	}

	/**
  * Set the dom value.
  * @param {dom} object - Main DOM element assigned to this instance.
  */


	/**
  * Update position of this instance based on the dom set.
  * @return {boolean} - Return true if dom is set and obtaining position was possible or false if dom was not set.
  */
	CommonFormMethods.prototype.updatePosition = function updatePosition() {
		if (this.dom) {
			this.position = CommonFormFunctions.getElementPosition(this.dom, false);
			return true;
		}
		return false;
	};

	_createClass(CommonFormMethods, [{
		key: "dom",
		set: function set(object) {
			this.domModule = object;
		}

		/**
   * Set the position value.
   * @param {Object} position - Position Object with top, right, bottom and left keys to be assigned to this instance.
   */
		,


		/**
   * Get the dom value.
   * @return {dom} - The assigned dom value.
   */
		get: function get() {
			return this.domModule || null;
		}

		/**
   * Get the name of this instance based on id and index, if present.
   * @return {string} - The name.
   */

	}, {
		key: "position",
		set: function set(position) {
			this.optionPosition = position;
		}

		/**
   * Set the index value.
   * @param {number} index - Index value for this instance.
   */
		,


		/**
   * Get position.
   * @return {object} - Item's position, with keys top, right, bottom and left.
   */
		get: function get() {
			return this.optionPosition || null;
		}

		/**
   * Get index.
   * @return {(number|null)} - Item's index if set, or null.
   */

	}, {
		key: "index",
		set: function set(index) {
			this.indexVal = index;
		},
		get: function get() {
			return !isNaN(this.indexVal) ? this.indexVal : null;
		}

		/**
   * Find whether this items stands alone or is in the list with other items - as the assign type is.
   * @return {boolean} - Return true if this item is alone or false if there are others present as well.
   */

	}, {
		key: "name",
		get: function get() {
			if (this.queryData.type === "radio" || this.queryData.type === "checkbox") {
				return this.queryData.id || "id-undefined";
			} else if (!this.standsAlone) {
				return this.queryData.id + String(this.index) || "id-undefined";
			}
			return this.queryData.id || "id-undefined";
		}

		/**
   * Get the id name of this instance based on index, if present.
   * @return {string} - The unique id value.
   */

	}, {
		key: "htmlId",
		get: function get() {
			var formIndex = this.queryData.formIndex !== null ? this.queryData.formIndex + "-" : "";
			if (!this.standsAlone) {
				return formIndex + this.currentSubItem.htmlId || "id-undefined";
			}
			return formIndex + this.queryData.id || "id-undefined";
		}
	}, {
		key: "standsAlone",
		get: function get() {
			return this.queryData.items.length === 0 || this.index === null;
		}

		/**
   * Get QueryDataBlock data for current subitem - that is in case there are other items present, or get the whole QueryDataBlock.
   * @return {(Object|QueryDataBlock)} - Return object for a subitem or QueryDataBlock for a standalone item.
   */

	}, {
		key: "currentSubItem",
		get: function get() {
			if (!this.standsAlone && this.queryData.items.length > 0) {
				return this.queryData.items[this.index];
			}
			return this.queryData;
		}
	}]);

	return CommonFormMethods;
}();