## Code Example

To create a checkbox qurey you'll need to define it on an existing DT form like this:

```
exampleform.newQueryItem({
	id: "checkbox",
	type: "checkbox",
	title: "Check all native European languages",
	subtitle: "One item allows for both possible check states",
	label: "Items:",
	error: "Please try again",
	items: [
		{ label: "English:", id: "english", checkRules: true },
		{ label: "German:", id: "german", checkRules: true },
		{ label: "Chinese:", id: "chinese", checkRules: false, error: "Really?" },
		{ label: "Czech:", id: "czech", checkRules: true },
		{ label: "Arabic:", id: "arabic", checkRules: false, error: "Really?" },
		{ label: "Turkish:", id: "turkish", checkRules: [true, false] },
		{ label: "Slovak:", id: "slovak", checkRules: true },
		{ label: "Greek:", id: "greek", checkRules: true }
	],
	css: "color: #000; background: #00bcd4;"
});
```

### Using HTML plugin the code can look like this:

```
<h3>Check all native European languages</h3>
<dtwrapper data-dttype="checkbox" data-dtsubtitle="One item allows for both possible check states" style="color: #000; background: #00bcd4;" data-dterror="Please try again">
	<input type="checkbox" name="laguage" value="english" data-dtlabel="English" data-dtcheckrules="true">
	<input type="checkbox" name="laguage" value="german" data-dtlabel="German" data-dtcheckrules="true">
	<input type="checkbox" name="laguage" value="chinese" data-dtlabel="Chinese" data-dtcheckrules="false" data-dterror="Really?">
	<input type="checkbox" name="laguage" value="czech" data-dtlabel="Czech" data-dtcheckrules="true">
	<input type="checkbox" name="laguage" value="arabic" data-dtlabel="Arabic" data-dtcheckrules="false" data-dterror="Really?">
	<input type="checkbox" name="laguage" value="turkish" data-dtlabel="Turkish" data-dtcheckrules="true, false">
	<input type="checkbox" name="laguage" value="slovak" data-dtlabel="Slovak" data-dtcheckrules="true">
	<input type="checkbox" name="laguage" value="greek" data-dtlabel="Greek" data-dtcheckrules="true">
</dtwrapper>
```

## Preview

<p align="center">
  <img src="checkbox.png" alt="radio query"/>
</p>


