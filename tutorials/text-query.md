## Code Example

To create a text qurey you'll need to define it on an existing DT form like this:

```
exampleform.newQueryItem({
	id: "name",
	type: "text",
	title: "What is your name?",
	subtitle: "Please fill in your name, like: \"John Mike\" (without quotes and accents).",
	label: "Write your name here:",
	checkRules: "/^[a-zA-Z ]+$/u",
	required: true,
	error: "This is not a name, please try it again. The correct characters are UTF-8 only.",
	css: "background: #f44336; color: #fff;"
});
```

### Using HTML plugin the code can look like this:

```
<h3>What is your name?</h3>
<dtwrapper data-dttype="text" data-dterror="This is not a name" data-dtsubtitle="Please fill in your name, like: 'John Mike' (without quotes and accents)." style="background: #f44336; color: #fff;">
	<input id="name" data-dtlabel="Name" data-dtcheckrules="/^[a-zA-ZàáâäãåąčćęèéêëėįìíîïłńòóôöõøùúûüųūÿýżźñçčšžÀÁÂÄÃÅĄĆČĖĘÈÉÊËÌÍÎÏĮŁŃÒÓÔÖÕØÙÚÛÜŲŪŸÝŻŹÑßÇŒÆČŠŽ∂ð ,.'-]+$/u" required>
</dtwrapper>
```

## Preview

<p align="center">
  <img src="text.png" alt="text query"/>
</p>


