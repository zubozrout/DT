## CSS Requirements

To make the library work some CSS may be required. The most important ones are as follows:

```
.dt-section-container .dt-section-container-input.dt-section-container-input-type-order {
	display: block;
	padding: 0px;
	width: 100%;
	height: 320px;
}

.dt-section-container .dt-section-container-input.dt-section-container-input-type-order.vertical {
	height: 640px;
}

.dt-section-container .dt-section-container-input-type-assign {
	display: block;
	width: 100%;
	height: 160px;
}

.dt-section-container .dt-section-container-options .option-draggable {
	display: inline-block;
	width: 128px;
	height: 128px;
}

.dt-section-container .dt-section-container-options .dt-options-clearer {
	clear: both;
}

.dt-section-container .dt-section-container-options .option-draggable.is-inside-container {
	border-radius: 100%;
}
```

It is important to make the input boxes larger than draggable items (.option-draggable). This applies at least for the input type order and assing.

Some less important features like options-clearer and snap indicator are listed here as well as they are a good thing to have as well.

The DT library supports many more features by setting distinct class names to a different elements of the form. To see more of what is supportet please head to the library example CSS stylesheet or check out the structure and behviour of a generated form.
