## Code Example

To create a ordering qurey you'll need to define it on an existing DT form like this:

```
exampleform.newQueryItem({
	id: "order-by-brightness",
	type: "order",
	direction: "horizontal",
	title: "Order the following things by its brightness.",
	subtitle: "The brihtest items should be on the left, the darkest on the right.",
	label: "Order here:",
	checkRules: ["runner", "man", "skunk", "dog", "mother-baby", "graduate", "tractor", "airplane", "witch", "crow"],
	error: "This is not in the right order.",
	options: [
		{ id: "airplane", text: "Airplane", css: "background-color: #ef6c00; background-image: url('../images/airplane-takeoff.png'); " + commonOptionCss },
		{ id: "tractor", text: "Tractor", css: "background-color: #f57c00; background-image: url('../images/tractor.png'); " + commonOptionCss },
		{ id: "crow", text: "Crow", css: "background-color: #e23900; background-image: url('../images/crow.png'); " + commonOptionCss },
		{ id: "dog", text: "Dog", css: "background-color: #ffb74d; background-image: url('../images/dog.png'); " + commonOptionCss },
		{ id: "mother-baby", text: "Mother with baby", css: "background-color: #ff9800; background-image: url('../images/mother-baby.png'); " + commonOptionCss },
		{ id: "graduate", text: "Graduate", css: "background-color: #fb8c00; background-image: url('../images/graduate.png'); " + commonOptionCss },
		{ id: "man", text: "Man", css: "background-color: #ffe0b2; background-image: url('../images/man.png'); " + commonOptionCss },
		{ id: "runner", text: "Runner", css: "background-color: #fff3e0; background-image: url('../images/runner.png'); " + commonOptionCss },
		{ id: "witch", text: "Witch", css: "background-color: #e65100; background-image: url('../images/witch.png'); " + commonOptionCss },
		{ id: "skunk", text: "Skunk", css: "background-color: #ffcc80; background-image: url('../images/skunk.png'); " + commonOptionCss }
	],
	optionsOrder: "after",
	after: "<em>Comparing two shades by putting them next to each other may help.</em>",
	css: "background: #673ab7; color: #fff;"
});
```

### Using HTML plugin the code can look like this:

```
<h3>Order the following things by its brightness.</h3>
<dtwrapper data-dttype="order" data-dtsubtitle="The brihtest items should be on the left, the darkest on the right." style="background: #673ab7; color: #fff;">
	<input name="order-by-brightness" data-dtdirection="horizontal" data-dtcheckrules="runner, man, skunk, dog, mother-baby, graduate, tractor, airplane, witch, crow">
	<select>
		<option value="airplane" style="background: #ef6c00; color: #000;">Airplane</option>
		<option value="tractor" style="background: #f57c00; color: #000;">Tractor</option>
		<option value="crow" style="background: #e23900; color: #fff;">Crow</option>
		<option value="dog" style="background: #ffb74d; color: #000;">Dog</option>
		<option value="mother-baby" style="background: #ff9800; color: #000;">Mother with baby</option>
		<option value="graduate" style="background: #fb8c00; color: #000;">Graduate</option>
		<option value="man" style="background: #ffe0b2; color: #000;">Man</option>
		<option value="runner" style="background: #fff3e0; color: #000;">Runner</option>
		<option value="witch" style="background: #e65100; color: #fff;">Witch</option>
		<option value="skunk" style="background: #ffcc80; color: #000;">Skunk</option>
	</select>
</dtwrapper>
```

## Preview

<p align="center">
  <img src="order.png" alt="radio query"/>
</p>


