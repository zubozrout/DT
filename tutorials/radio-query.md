## Code Example

To create a radio qurey you'll need to define it on an existing DT form like this:

```
exampleform.newQueryItem({
	id: "gender",
	type: "radio",
	title: "What's your gender?",
	subtitle: "Pick one of the below options",
	label: "Options:",
	required: true,
	items: [
		{ label: "Male:", id: "male", text: "Male" },
		{ label: "Female:", id: "female", text: "Female" },
		{ label: "Other:", id: "other", text: "Other" }
	],
	css: "background: #9c27b0; color: #fff;"
});
```

### Using HTML plugin the code can look like this:

```
<h3>What's your gender?</h3>
<dtwrapper data-dttype="radio" data-dtsubtitle="Pick one of the below options" style="background: #9c27b0; color: #fff;">
	<input type="radio" name="gender" value="male" data-dtlabel="Male" required>
	<input type="radio" name="gender" value="female" data-dtlabel="Female">
	<input type="radio" name="gender" value="other" data-dtlabel="Other">
</dtwrapper>
```

## Preview

<p align="center">
  <img src="radio.png" alt="radio query"/>
</p>


