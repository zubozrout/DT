## Code Example

To create a assigning qurey you'll need to define it on an existing DT form like this:

```
exampleform.newQueryItem({
	id: "join",
	type: "assign",
	title: "Put the following items to the right categories.",
	subtitle: "Some categories have more than one possible answer.",
	items: [
		{ label: "Human:", id: "human", checkRules: ["man", "baby", "volleyball-player"], arrayMatch: "and", error: "Not all and only human beings." },
		{ label: "Animal:", id: "animal", checkRules: ["horse", "skunk"], arrayMatch: "and", error: "Not all and only animals." },
		{ label: "Vehicle:", id: "vehicle", checkRules: ["airplane", "tractor"], arrayMatch: "and", error: "Not all and only vehicles." },
		{ label: "Goods:", id: "goods", checkRules: "bottle", arrayMatch: "and", error: "Not all and only goods." }
	],
	options: [
		{ id: "airplane", text: "Airplane", css: "font-size: 0px; background-color: #fff; background-image: url('../images/airplane.png'); " + commonOptionCss },
		{ id: "man", text: "Man", css: "font-size: 0px; background-color: #fff; background-image: url('../images/man.png'); " + commonOptionCss },
		{ id: "baby", text: "Baby", css: "font-size: 0px; background-color: #fff; background-image: url('../images/baby.png'); " + commonOptionCss },
		{ id: "horse", text: "Horse", css: "font-size: 0px; background-color: #fff; background-image: url('../images/horse.png'); " + commonOptionCss },
		{ id: "tractor", text: "Tractor", css: "font-size: 0px; background-color: #fff; background-image: url('../images/tractor.png'); " + commonOptionCss },
		{ id: "volleyball-player", text: "Volleyball Player", css: "font-size: 0px; background-color: #fff; background-image: url('../images/volleyball-player.png'); " + commonOptionCss },
		{ id: "skunk", text: "Skunk", css: "font-size: 0px; background-color: #fff; background-image: url('../images/skunk.png'); " + commonOptionCss },
		{ id: "bottle", text: "Bottle", css: "font-size: 0px; background-color: #fff; background-image: url('../images/bottle.png'); " + commonOptionCss }
	],
	optionsOrder: "before",
	css: "color: #fff; background: #2196f3;"
});
```

### Using HTML plugin the code can look like this:

```
<h3>Put the following items to the right categories.</h3>
<dtwrapper data-dttype="assign" data-dtsubtitle="Some categories have more than one possible answer." style="color: #fff; background: #2196f3;">
	<input name="human" data-dtlabel="Human" data-dtcheckrules="man, baby, volleyball-player" data-dtarraymatch="and" data-dterror="Not all and only human beings.">
	<input name="animal" data-dtlabel="Animal" data-dtcheckrules="horse, skunk" data-dtarraymatch="and" data-dterror="Not all and only animals.">
	<input name="vehicle" data-dtlabel="Vehicle" data-dtcheckrules="airplane, tractor" data-dtarraymatch="and" data-dterror="Not all and only vehicles.">
	<input name="goods" data-dtlabel="Goods" data-dtcheckrules="bottle" data-dtarraymatch="and" data-dterror="Not all and only goods.">
	<select>
		<option value="airplane" style="font-size: 0px; background-color: #fff; background-image: url('../images/airplane.png'); background-size: contain; bakground-position: center center;">Airplane</option>
		<option value="man" style="font-size: 0px; background-color: #fff; background-image: url('../images/man.png'); background-size: contain; bakground-position: center center;">Man</option>
		<option value="baby" style="font-size: 0px; background-color: #fff; background-image: url('../images/baby.png'); background-size: contain; bakground-position: center center;">Baby</option>
		<option value="horse" style="font-size: 0px; background-color: #fff; background-image: url('../images/horse.png'); background-size: contain; bakground-position: center center;">Horse</option>
		<option value="tractor" style="font-size: 0px; background-color: #fff; background-image: url('../images/tractor.png'); background-size: contain; bakground-position: center center;">Tractor</option>
		<option value="volleyball-player" style="font-size: 0px; background-color: #fff; background-image: url('../images/volleyball-player.png'); background-size: contain; bakground-position: center center;">Volleyball Player</option>
		<option value="skunk" style="font-size: 0px; background-color: #fff; background-image: url('../images/skunk.png'); background-size: contain; bakground-position: center center;">Skunk</option>
		<option value="bottle" style="font-size: 0px; background-color: #fff; background-image: url('../images/bottle.png'); background-size: contain; bakground-position: center center;">Bottle</option>
	</select>
</dtwrapper>
```

## Preview

<p align="center">
  <img src="assign.png" alt="radio query"/>
</p>


