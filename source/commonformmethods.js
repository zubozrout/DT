"use strict";

/**
 * An abstract class containing some suited functionality widely used in other spedific DT classes.
 * @abstract
 */
class CommonFormMethods {
	/**
	 * Create CommonFormMethods.
	 */
	constructor() {
		/* http://stackoverflow.com/a/30560792 */
		if(new.target === CommonFormMethods) {
			throw new TypeError("Cannot construct CommonFormMethods instance directly, abstract class");
		}
		return this;
	}
	
	/**
	 * Set the dom value.
	 * @param {dom} object - Main DOM element assigned to this instance.
	 */
	set dom(object) {
		this.domModule = object;
	}
	
	/**
	 * Set the position value.
	 * @param {Object} position - Position Object with top, right, bottom and left keys to be assigned to this instance.
	 */
	set position(position) {
		this.optionPosition = position;
	}
	
	/**
	 * Set the index value.
	 * @param {number} index - Index value for this instance.
	 */
	set index(index) {
		this.indexVal = index;
	}
	
	/**
	 * Get the dom value.
	 * @return {dom} - The assigned dom value.
	 */
	get dom() {
		return this.domModule || null;
	}
	
	/**
	 * Get the name of this instance based on id and index, if present.
	 * @return {string} - The name.
	 */
	get name() {
		if(this.queryData.type === "radio" || this.queryData.type === "checkbox") {
			return this.queryData.id || "id-undefined";
		}
		else if(!this.standsAlone) {
			return (this.queryData.id + String(this.index)) || "id-undefined";
		}
		return this.queryData.id || "id-undefined"
	}
	
	/**
	 * Get the id name of this instance based on index, if present.
	 * @return {string} - The unique id value.
	 */
	get htmlId() {
		let formIndex = this.queryData.formIndex !== null ? this.queryData.formIndex + "-" : "";
		if(!this.standsAlone) {
			return formIndex + this.currentSubItem.htmlId || "id-undefined";
		}
		return formIndex + this.queryData.id || "id-undefined"
	}
	
	/**
	 * Get position.
	 * @return {object} - Item's position, with keys top, right, bottom and left.
	 */
	get position() {
		return this.optionPosition || null;
	}
	
	/**
	 * Get index.
	 * @return {(number|null)} - Item's index if set, or null.
	 */
	get index() {
		return !isNaN(this.indexVal) ? this.indexVal : null;
	}
	
	/**
	 * Find whether this items stands alone or is in the list with other items - as the assign type is.
	 * @return {boolean} - Return true if this item is alone or false if there are others present as well.
	 */
	get standsAlone() {
		return this.queryData.items.length === 0 || this.index === null;
	}
	
	/**
	 * Get QueryDataBlock data for current subitem - that is in case there are other items present, or get the whole QueryDataBlock.
	 * @return {(Object|QueryDataBlock)} - Return object for a subitem or QueryDataBlock for a standalone item.
	 */
	get currentSubItem() {
		if(!this.standsAlone && this.queryData.items.length > 0) {
			return this.queryData.items[this.index];
		}
		return this.queryData;
	}
	
	/**
	 * Update position of this instance based on the dom set.
	 * @return {boolean} - Return true if dom is set and obtaining position was possible or false if dom was not set.
	 */
	updatePosition() {
		if(this.dom) {
			this.position = CommonFormFunctions.getElementPosition(this.dom, false);
			return true;
		}
		return false;
	}
}
