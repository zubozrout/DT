#!/bin/sh

# To run this script you have to have uglify-js-harmony installed. In case the below paths do not match feel free to chnage them for your instance.

MINNAME="dt.min.js"
BABELIZEDNAME="dt-babelized.js"

SOURCE="source"
if [ -d "$SOURCE" ]; then
	# join and minify ES6:
	~/node_modules/.bin/uglifyjs $SOURCE/*.js > $MINNAME
	sleep 1
	# keep only the first "use strict";, don't repeat.
	sed -i -- 's/"use strict";//2g' $MINNAME
fi

BABELIZED="babelized"
if [ -d "$BABELIZED" ]; then
	# join Babelized:
	echo "" > $BABELIZEDNAME
	cat $BABELIZED/*.js >> $BABELIZEDNAME
	sleep 1
	
	# Fix Abstract class definition (remove it):
	sleep 1
	sed -i -- 's/new.target === CommonFormMethods/false/g' $BABELIZEDNAME
	sleep 1
	sed -i -- 's/new.target === CommonFormFunctions/false/g' $BABELIZEDNAME
fi
